<?php
class myValidatorDoctrineChoiceNestedSet extends sfValidatorBase
{
  /**
   * Configures the validator.
   * Available options:
   *   model: The model class (required)
   *   node:   The node being moved (required)
   *
   * @see sfValidatorBase
   */
  protected function configure($options = array(), $messages = array())
  {
    $this->addRequiredOption('model');
    $this->addRequiredOption('node');

    $this->addMessage('node', 'A node cannot be set as a child of itself.');
  }

  protected function doClean($value)
  {
    return $value;
//    print_r($value); die();
    $me = $value['parent_id'];
    if (isset($value) && !$value)
    {
      unset($value);
    }
    else
    {
      $targetNode = Doctrine::getTable($this->getOption('model'))->find($me)->getNode();
      if ($targetNode->isDescendantOfOrEqualTo($this->getOption('node')))
      {
        throw new sfValidatorError($this, 'node', array('value' => $me));
      }

      return $value;
    }
  }
}
