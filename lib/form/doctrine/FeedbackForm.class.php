<?php

/**
 * Feedback form.
 *
 * @package    itworks
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class FeedbackForm extends BaseFeedbackForm
{
    public function configure()
    {
        unset(
            #$this[''],
            $this['created_at'],
            $this['updated_at']
        );

        sfValidatorBase::setDefaultMessage('required', 'обязательное поле');

        $this->setWidget('subject', new sfWidgetFormInputText());

        $this->setWidget('content', new sfWidgetFormTextarea(
            array('label' => 'Вопрос'),
            array('class' => 'formTexarea')
        ));
        $this->setValidator('content', new sfValidatorString(array('required' => true)));

        $this->setWidget('captcha', new sfWidgetFormInputText());

//    $this->setWidget('antispam', new sfWidgetFormPHPCaptcha());
//    $this->setValidator('antispam', new sfValidatorPHPCaptcha(
//      array('required' => true)
//    ));

        $this->setValidator('email', new sfValidatorEmail());

        $this->validatorSchema->setPostValidator(new sfValidatorOr(array(
                new sfValidatorSchemaCompare('email', '!=', ''),
                new sfValidatorSchemaCompare('phone', '!=', ''),
            ),
            array(),
            array('invalid' => 'Необходимо заполнить телефон или Email')
        ));

        $this->widgetSchema->setLabels(array(
            'name' => 'Ф.И.О.',
            'phone' => 'Телефон',
            'subject' => 'Тема сообщения',
        ));

        //mark required fields
        foreach ($this->getFormFieldSchema()->getWidget()->getFields() as $key => $object) {
            $label = $this->getFormFieldSchema()->offsetGet($key)->renderLabelName();
            if ($this->validatorSchema[$key])
                if ($this->validatorSchema[$key]->getOption('required') == true) {
                    $this->widgetSchema->setLabel($key, $label . "<span class='markRequiredField'>*</span>");
                }
        }

        $this->validatorSchema->setOption('allow_extra_fields', true);
        $this->validatorSchema->setOption('filter_extra_fields', false);
    }
}
