<?php

/**
 * Banner form.
 *
 * @package    scp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id$
 */
class BannerForm extends BaseBannerForm
{
    public function configure()
    {

        $picPath = implode('/', array(
            'uploads',
            'promo',
            $this->getObject()->get('pic')
        ));

        $uploadDirName = implode('/', array(
            sfConfig::get('sf_upload_dir'),
            'promo',
        ));

        $this->setWidget('pic', new aceWidgetFormInputFileEditable([
            'label' => 'Picture',
            'file_src' => $picPath,
            'is_image' => true,
            'edit_mode' => ($this->getObject()->get('pic') AND !$this->getObject()->isNew() ? true : false),
            'template' => '
                        <table>
                            <tr>
                                <td>
                                    %input% <br /> %delete% delete file
                                </td>
                                <td>
                                        <a rel="extraPics" title="" href="/' . $picPath . '" target="_blank"><img
                                        src="' . thumbnail_path($picPath, 50, 50, 'promo') . '" /></a>
                            </tr>
                        </table>',
        ], [
            'class' => 'ace'
        ]));

        $this->setValidator(
            'pic', new sfValidatorFile(array(
                'required' => false,
                'path' => '/' . $uploadDirName,
                'max_size' => '10240000', // bytes (1MB)
                'mime_types' => array(
                    'image/gif',
                    'image/jpeg',
                    'image/png'
                )
            )));
    }
}
