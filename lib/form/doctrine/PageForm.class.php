<?php

/**
 * Page form.
 *
 * @package    scp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id$
 */
class PageForm extends BasePageForm
{
    public function configure()
    {
        $this->setWidget('extra_meta', new sfWidgetFormTextarea([], ['cols' => 80, 'rows' => 6]));
        $this->setWidget('announce', new sfWidgetFormTextarea([], ['cols' => 80, 'rows' => 2]));

        $picPath = implode('/', array(
            'uploads',
            'attach',
            $this->getObject()->get('attach')
        ));
        $uploadDirName = implode('/', array(
            sfConfig::get('sf_upload_dir'),
            'attach',
        ));


        $this->setWidget('attach', new aceWidgetFormInputFileEditable([
            'file_src' => $picPath,
            'is_image' => true,
            'edit_mode' => ($this->getObject()->get('attach') AND !$this->getObject()->isNew() ? true : false),
            'template' => '
                        <table>
                            <tr>
                                <td>
                                    %input%
                                    <a href="/' . $picPath . '" target="_blank">' . $this->getObject()->get('attach') . '</a>
                                    <br />
                                    %delete% delete file
                                </td>
                            </tr>
                        </table>',
        ], [
            'class' => 'ace'
        ]));

        $this->setValidator('attach', new sfValidatorFile(array(
            'required' => false,
            'path' => '/' . $uploadDirName,
            'max_size' => '10240000', // bytes (1MB)
            'mime_types' => array(
                'application/pdf',
                'application/x-pdf',
                'application/vnd.openxmlformats-officedocument.presentationml.slideshow',
            )
        )));
        $this->setValidator('attach_delete', new sfValidatorPass()); // pic_delete
    }

    public function generateAttachFileName($file)
    {
        return $file->getOriginalName();
    }
}
