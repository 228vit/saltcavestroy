<?php

/**
 * Project form base class.
 *
 * @package    scp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id$
 */
abstract class BaseFormDoctrine extends sfFormDoctrine
{
    public function setup()
    {
        ProjectConfiguration::getActive()->loadHelpers(array('Thumbnail', 'Asset'));

        unset($this['created_at'], $this['updated_at']);

        // checkboxes
        $booleans = array('allow_comments', 'view_in_slider', 'announce_on_top', 'is_show_on_top', 'is_active', 'is_checked', 'is_translated', 'is_published');
        foreach ($booleans as $announceFieldName) {
            if (isSet($this[$announceFieldName])) {
                $this->widgetSchema[$announceFieldName] = new aceWidgetFormInputCheckbox();
            } // announce
        } // each announce field


        if (isSet($this['published_at'])) {
            $years = range(
                (int)date('Y') - sfConfig::get('app_date_widget_years_before', 2),
                (int)date('Y') + sfConfig::get('app_date_widget_years_after', 2)
            ); //Creates array of years
            $years_list = array_combine($years, $years); //Creates new array where key and value are both values from $years list

            $this->setWidget('published_at', new aceWidgetFormDate());
            if ($this->getObject()->isNew()) {
                $this->widgetSchema->setDefault('published_at', date('Y-m-d'));
            }
        } // published at

        $announces = array('caption', 'announce', 'extract', 'note', 'meta_keywords', 'extra_meta', 'meta_description', 'explanation');
        foreach ($announces as $announceFieldName) {
            if (isSet($this[$announceFieldName])) {
                $this->widgetSchema[$announceFieldName] = new sfWidgetFormTextarea(array(), array('style' => 'width: 550px; height: 80px;'));
            } // announce
        } // each announce field

        $contents = array('body', 'note');
        foreach ($contents as $announceFieldName) {
            if (isSet($this[$announceFieldName])) {
                $this->widgetSchema[$announceFieldName] = new sfWidgetFormTextarea(array(), array('style' => 'width: 400px; height: 100px;'));
            } // announce
        } // each announce field

        $contents = array('content', 'description');
        if (class_exists('CKEditor')) {

            foreach ($contents as $contentFieldName) {
                if (isSet($this[$contentFieldName])) {
                    $this->widgetSchema[$contentFieldName] = new sfWidgetFormCKEditor();
                    $finder = $this->widgetSchema[$contentFieldName]->getFinder();

                    $editor = $this->widgetSchema[$contentFieldName]->getEditor();
                    $editor->config['toolbar'] = array(
                        array('Source', '-', 'Save', '-', 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-'),
                        array('Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'Format', 'Font', 'FontSize', 'TextColor', 'RemoveFormat'),
                        array('-', 'NumberedList', 'BulletedList', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', '-', 'Blockquote'),
                        array('Link', 'Unlink', 'Maximize', 'ShowBlocks', '-', 'Image'),
                    );
                    // ,'Outdent','Indent','-'
                    $editor->config['height'] = '200px';
                } // content
            } // each content field
        }

        $pics = array('pic', 'big_pic', 'picture', 'thumb', 'img', 'image', 'photo', 'avatar');

        foreach ($pics as $picFieldName) {
            if (isSet($this[$picFieldName])) {
//        if (is_callable('getPicDir', true))
                if (method_exists($this, 'getPicDir')) {
                    $modelName = $this->getPicDir();
                } else if (is_callable('getModelName', true, $modelName)) {
                    // TODO: fix this, make it editable
                    $modelName = strtolower($this->getModelName());
                    $modelName = myConfig::get($modelName . '_dir', $modelName);
                } else {
                    $modelName = false;
                }
                $uploadDirName = implode('/', array(
                    sfConfig::get('sf_upload_dir'),
                    $modelName,
                ));

                $picPath = implode('/', array(
                    'uploads',
                    $modelName,
                    $this->getObject()->get($picFieldName)
                ));

                $this->setWidget($picFieldName, new aceWidgetFormInputFileEditable([
                    'label' => ucfirst($picFieldName),
                    'file_src' => $picPath,
                    'is_image' => true,
                    'edit_mode' => ($this->getObject()->get($picFieldName) AND !$this->getObject()->isNew() ? true : false),
                    'template' => '
                        <table>
                            <tr>
                                <td>
                                    %input% <br /> %delete% delete file
                                </td>
                                <td>
                                        <a rel="extraPics" title="" href="/' . $picPath . '" target="_blank"><img
                                        src="' . thumbnail_path($picPath, 50, 50, $modelName) . '" /></a>
                            </tr>
                        </table>',
                ], [
                    'class' => 'ace'
                ]));

                $this->setValidator(
                    $picFieldName, new sfValidatorFile(array(
                        'required' => false,
                        'path' => '/' . $uploadDirName,
                        'max_size' => '10240000', // bytes (1MB)
                        'mime_types' => array(
                            'image/gif',
                            'image/jpeg',
                            'image/png'
                        )
                    )));
                $this->setValidator($picFieldName . '_delete', new sfValidatorPass()); // pic_delete

            } // if pic
        } // foreach pics
//        foreach ($labels as $fieldName => $label) {
//            if (isSet($this[$fieldName])) {
//                $this->widgetSchema->setLabel($fieldName, $label);
//            }
//        } // foreach ($labels as $fieldName)

        $this->markRequired();

    } // setup

    public function markRequired()
    {
        //mark required fields
        foreach ($this->getFormFieldSchema()->getWidget()->getFields() as $key => $object) {
            $label = $this->getFormFieldSchema()->offsetGet($key)->renderLabelName();
            if ($this->validatorSchema[$key])
                if ($this->validatorSchema[$key]->getOption('required') == true) {
                    $this->widgetSchema[$key]->setAttribute('required', true);
                    $this->widgetSchema->setLabel($key, $label . "<span class='required'>*</span>");
                }
        } // mark required fields
    }


    /**
     * Embed a Doctrine_Collection relationship in to a form
     *
     *     [php]
     *     $userForm = new UserForm($user);
     *     $userForm->embedRelation('Groups AS groups');
     *
     * @param  string $relationName The name of the relation and an optional alias
     * @param  string $formClass The name of the form class to use
     * @param  array $formArguments Arguments to pass to the constructor (related object will be shifted onto the front)
     * @param string $innerDecorator A HTML decorator for each embedded form
     * @param string $decorator A HTML decorator for the main embedded form
     *
     * @throws InvalidArgumentException If the relationship is not a collection
     */
    public function myEmbedRelation($relationName, $formClass = null, $formArgs = array(), $innerDecorator = null, $decorator = null)
    {
        if (false !== $pos = stripos($relationName, ' as ')) {
            $fieldName = substr($relationName, $pos + 4);
            $relationName = substr($relationName, 0, $pos);
        } else {
            $fieldName = $relationName;
        }

        $relation = $this->getObject()->getTable()->getRelation($relationName);

        $r = new ReflectionClass(null === $formClass ? $relation->getClass() . 'Form' : $formClass);

        if (Doctrine_Relation::ONE == $relation->getType()) {
            $this->embedForm($fieldName, $r->newInstanceArgs(array_merge(array($this->getObject()->$relationName), $formArgs)), $decorator);
        } else {
            $subForm = new sfForm();

            foreach ($this->getObject()->$relationName as $index => $childObject) {
                $form = $r->newInstanceArgs(array_merge(array($childObject), $formArgs));

                $subForm->embedForm($index, $form, $innerDecorator);
                $subForm->getWidgetSchema()->setLabel($index, $index + 1);
            }

            $this->embedForm($fieldName, $subForm, $decorator);
        }
    }

}
