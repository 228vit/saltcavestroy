<?php
require_once(sfConfig::get('sf_lib_dir') . '/vendor/PHPImageWorkshop/ImageWorkshop.php');
require_once(sfConfig::get('sf_lib_dir') . '/vendor/PHPImageWorkshop/Core/ImageWorkshopLib.php');
require_once(sfConfig::get('sf_lib_dir') . '/vendor/PHPImageWorkshop/Core/ImageWorkshopLayer.php');
require_once(sfConfig::get('sf_lib_dir') . '/vendor/PHPImageWorkshop/Exception/ImageWorkshopBaseException.php');
require_once(sfConfig::get('sf_lib_dir') . '/vendor/PHPImageWorkshop/Exception/ImageWorkshopException.php');
require_once(sfConfig::get('sf_lib_dir') . '/vendor/PHPImageWorkshop/Exception/ImageWorkshopException.php');
//require_once(sfConfig::get('sf_lib_dir').'/vendor/PHPImageWorkshop/');

use PHPImageWorkshop\ImageWorkshop;

function workshop_thumb($path, $maxWidth, $maxHeight)
{

    if (!file_exists($path)) {
        return "http://placehold.it/{$maxWidth}x{$maxHeight}";
    }

    $pic = ImageWorkshop::initFromPath($path);
    $pic->resizeInPixel($maxWidth, $maxHeight, true);

    $thumbsDir = sfConfig::get('sf_web_dir') . '/uploads/thumbs';
    $fileName = basename($path);
    $thumbName = $fileName . '-' . $maxWidth . 'x' . $maxHeight . '.jpg';
    $thumbPath = $thumbsDir . '/' . $thumbName;

    $pic->save($thumbsDir, $thumbName, $createFolders = false, $backgroundColor = null, $imageQuality = 90);

    return image_path('/uploads/thumbs/' . $thumbName);
}

function workshop_crop_no_border($path, $maxWidth, $maxHeight)
{

    // check if thumb already exists
    $thumbsDir = sfConfig::get('sf_web_dir') . '/uploads/thumbs';
    $fileName = basename($path);
    $thumbName = $fileName . '-' . $maxWidth . 'x' . $maxHeight . '.jpg';
    $thumbPath = $thumbsDir . '/' . $thumbName;

    if (file_exists($thumbPath)) {
        return image_path('/uploads/thumbs/' . $thumbName);
    }

    if (!file_exists($path)) {
        return "http://placehold.it/{$maxWidth}x{$maxHeight}";
    }

    $expectedWidth = $maxWidth;
    $expectedHeight = $maxHeight;
    
    $pic = ImageWorkshop::initFromPath($path);

    // Determine the largest expected side automatically
    ($expectedWidth > $expectedHeight) ? $largestSide = $expectedWidth : $largestSide = $expectedHeight;

    // Get a squared layer
    $pic->cropMaximumInPixel(0, 0, "MM");

    // Resize the squared layer with the largest side of the expected thumb
    $pic->resizeInPixel($largestSide, $largestSide);

    // Crop the layer to get the expected dimensions
    $pic->cropInPixel($expectedWidth, $expectedHeight, 0, 0, 'MM');

    $thumbsDir = sfConfig::get('sf_web_dir') . '/uploads/thumbs';
    $fileName = basename($path);
    $thumbName = $fileName . '-' . $maxWidth . 'x' . $maxHeight . '.jpg';

    $pic->save($thumbsDir, $thumbName, $createFolders = false, $backgroundColor = null, $imageQuality = 90);

    return image_path('/uploads/thumbs/' . $thumbName);
}

function workshop_square_thumb($path, $size)
{
    // check if thumb already exists
    $thumbsDir = sfConfig::get('sf_web_dir') . '/uploads/thumbs';
    $fileName = basename($path);
    $thumbName = $fileName . '-' . $size . 'x' . $size . '.jpg';
    $thumbPath = $thumbsDir . '/' . $thumbName;

    if (file_exists($thumbPath)) {
        return image_path('/uploads/thumbs/' . $thumbName);
    }

    if (!file_exists($path)) {
        return "http://placehold.it/{$size}x{$size}";
    }

    $pic = ImageWorkshop::initFromPath($path);
    // Get a square ("MM" to say in the middle of the layer) of the maximum possible
    $pic->cropMaximumInPixel(0, 0, "MM");
    $pic->resizeInPixel($size, $size);

    $thumbsDir = sfConfig::get('sf_web_dir') . '/uploads/thumbs';
    $fileName = basename($path);
    $thumbName = $fileName . '-' . $size . 'x' . $size . '.jpg';
    $thumbPath = $thumbsDir . '/' . $thumbName;

    $pic->save($thumbsDir, $thumbName, $createFolders = false, $backgroundColor = null, $imageQuality = 90);

    return image_path('/uploads/thumbs/' . $thumbName);
}


function checkFakeIconExists()
{
    // check if fake icon exists
    $fakeIcon = sfConfig::get('sf_web_dir') . '/uploads/fake-icon.jpg';
    if (!file_exists($fakeIcon) || !is_file($fakeIcon)) {
        $thumbsDir = sfConfig::get('sf_web_dir') . '/uploads/thumbs';
        if (!file_exists($thumbsDir)) {
            mkdir($thumbsDir);
            chmod($thumbsDir, 0777);
        }
        $img = new sfImage();
        $img->create(200, 200, '#FFFFFF');
        $img->setQuality(100);

        $img->saveAs($fakeIcon, 'image/jpg');
    }
}

function thumbnail_scale($path, $maxWidth, $maxHeight, $noPic = 'no-pic.jpg')
{

    $thumbsDir = sfConfig::get('sf_web_dir') . '/uploads/thumbs';
    if (!file_exists($thumbsDir)) {
        mkdir($thumbsDir);
        chmod($thumbsDir, 0777);
    }

    $path = ($path{0} == '/' ? '' : '/') . $path;

    // get source pic
    $sourcePath = sfConfig::get('sf_web_dir') . '/' . $path;

    // check if source file exists
    if (!file_exists($sourcePath) || !is_file($sourcePath)) {
        $sourcePath = $thumbsDir . '/' . $noPic;
        if (!file_exists($sourcePath) || !is_file($sourcePath)) {
            $img = new sfImage();
            $img->create(200, 200, '#A8A8A8');
            $img->setQuality(100);
            $img->saveAs($sourcePath, 'image/jpg');
        }
    }

    checkFakeIconExists();

    $i = new sfImage($sourcePath);
    // portrait 800/600 = 1.33
    $wProportion = round($i->getWidth() / $i->getHeight(), 4);
    // both cannot be null
    if (empty($maxWidth) && empty($maxHeight)) {
        $maxWidth = 200;
    }

    $maxHeight = (int)$maxHeight;
    $maxWidth = (int)$maxWidth;

    // check if some isNull
    $maxWidth = empty($maxWidth) ? round($maxHeight * $wProportion) : $maxWidth;
    $maxHeight = empty($maxHeight) ? round($maxWidth / $wProportion) : $maxHeight;

    $fileName = basename($path);
    $thumbName = $fileName . '-' . $maxWidth . 'x' . $maxHeight . '.jpg';
    $thumbPath = $thumbsDir . '/' . $thumbName;
    $destPath = $thumbPath;
    if (!file_exists($destPath)) {
        // ???
        $con = min($i->getWidth(), $i->getHeight());
        if ($i->getWidth() >= $i->getHeight()) {
            $maxWidth = $maxWidth;//$i->getWidth();
            $maxHeight = round($maxWidth / ($i->getWidth() / $i->getHeight()));
        } else {
            $maxHeight = $maxHeight;//$i->getHeight();
            $maxWidth = round($maxHeight / ($i->getHeight() / $i->getWidth()));
        }
        $i->resize($maxWidth, $maxHeight);
        $i->setQuality(90);

        $i2 = new sfImage(sfConfig::get('sf_web_dir') . '/uploads/fake-icon.jpg');
        $i2->resize($maxWidth, $maxHeight);
        $i2->overlay($i, 'middle-center'); // or you can use coords array($x,$y)

        $i2->saveAs($destPath, 'image/jpg');
    }

    return image_path('/uploads/thumbs/' . $thumbName);

}

function thumbnail_resize($path, $maxWidth, $maxHeight, $noPic = 'no-pic.jpg')
{
    $path = ($path{0} == '/' ? '' : '/') . $path;

    $fileName = basename($path);

    $thumbsDir = sfConfig::get('sf_web_dir') . '/uploads/thumbs';
    if (!file_exists($thumbsDir)) {
        mkdir($thumbsDir);
        chmod($thumbsDir, 0777);
    }

    $thumbName = $fileName . '-' . $maxWidth . 'x' . $maxHeight . '.jpg';
    $thumbPath = $thumbsDir . '/' . $thumbName;

    $sourcePath = sfConfig::get('sf_web_dir') . '/' . $path;
    $destPath = $thumbPath;

    if (!file_exists($sourcePath) || !is_file($sourcePath)) {
        $sourcePath = $thumbsDir . '/' . $noPic;
        if (!file_exists($sourcePath) || !is_file($sourcePath)) {
            $img = new sfImage();
            $img->create(200, 200, '#A8A8A8');
            $img->text('no pic', 10, 10, 20, 'Arial', '#FF0000');
            $img->setQuality(100);
            $img->saveAs($sourcePath, 'image/jpg');
        }
    }

    if (!file_exists($destPath)) {
        $i = new sfImage($sourcePath);

        $origProportion = round($i->getWidth() / $i->getHeight(), 4);
        // check if null
        $maxWidth = (int)$maxWidth;
        if (is_null($maxHeight)) {
            $maxHeight = round($maxWidth * $origProportion);
        }
        $maxHeight = (int)$maxHeight;
        $newProportion = round($maxWidth / $maxHeight, 4);

        // crop by width
        if ($i->getWidth() > $i->getHeight() && $newProportion < $origProportion) {
            $maxWidth = round($i->getHeight() * $newProportion);
            $maxHeight = $i->getHeight();
            $top = 0;
            $left = round(($i->getWidth() - $maxWidth) / 2);

//      die("$newProportion < $origProportion [$top, $left] :: $maxWidth, $maxHeight :: ".$i->getWidth()." - ".$i->getHeight());

            $i->crop($left, $top, $maxWidth, $maxHeight);
        } // crop by height
        elseif ($i->getWidth() < $i->getHeight() && $newProportion > $origProportion) //    elseif ($newProportion > $origProportion)
        {
            $maxHeight = round($i->getWidth() / $newProportion);
            $maxWidth = $i->getWidth();
            $top = round(($i->getHeight() - $maxHeight) / 2);
            $left = 0;
//      die("$newProportion < $origProportion [t $top, l $left] :: [[nw$maxWidth, nh$maxHeight]] :: w".$i->getWidth()." - h".$i->getHeight());

            $i->crop($left, $top, $maxWidth, $maxHeight);
        }

        $i->thumbnail($maxWidth, $maxHeight);
        $i->setQuality(100);
        $i->saveAs($destPath, 'image/jpg');

        return image_path('/uploads/thumbs/' . $thumbName);

        //

        $con = min($i->getWidth(), $i->getHeight());

        if ($i->getWidth() > $i->getHeight()) {
            $left = round(($i->getWidth() - $i->getHeight()) / 2);
            $top = 0;
        } elseif ($i->getWidth() < $i->getHeight()) {
            $left = 0;
            #by vit
            #$top = $i->getHeight() / 2 - $i->getWidth();
            $top = round(($i->getHeight() - $i->getWidth()) / 2);
        } else {
            $left = $top = 0;
        }

        //crop if pic bigget than neede icon
        if ($con > min($maxWidth, $maxHeight)) {
//      $proportion = round($i->getWidth()/$i->getHeight(), 2);
            $proportion = round($maxWidth / $maxHeight, 3);
            if ($i->getWidth() > $i->getHeight()) {
                $maxWidth = round($i->getWidth() / $proportion);
                $maxHeight = round($i->getHeight() / $proportion);

            }
            die("$proportion $maxWidth, $maxHeight");
            $i->crop($left, $top, $maxWidth, $maxHeight);
        } else {
            //die('overlay');
            //use overlay
            $i = new sfImage(sfConfig::get('sf_web_dir') . '/uploads/fake-icon.jpg');
            $i->overlay(new sfImage($sourcePath), 'middle-center'); // or you can use coords array($x,$y)
            //$i->text('over', 10, 10, 20, '', '#000000');


        }

        $i->thumbnail($maxWidth, $maxHeight);
        $i->setQuality(100);
        $i->saveAs($destPath, 'image/jpg');
    }

    return image_path('/uploads/thumbs/' . $thumbName);
}

function thumbnail_path($path, $maxWidth, $maxHeight, $noPic = 'no-pic.jpg')
{
//  die($path);
    $path = ($path{0} == '/' ? '' : '/') . $path;

    $maxWidth = (int)$maxWidth;
    $maxHeight = (int)$maxHeight;

    $fileName = basename($path);

    $thumbsDir = sfConfig::get('sf_web_dir') . '/uploads/thumbs';
    if (!file_exists($thumbsDir)) {
        mkdir($thumbsDir);
        chmod($thumbsDir, 0777);
    }

    $thumbName = $fileName . '-' . $maxWidth . 'x' . $maxHeight . '.jpg';
    $thumbPath = $thumbsDir . '/' . $thumbName;

    $sourcePath = sfConfig::get('sf_web_dir') . '/' . $path;
    $destPath = $thumbPath;

    if (!file_exists($sourcePath) || !is_file($sourcePath)) {
        $sourcePath = $thumbsDir . '/no-pic.jpg';
        if (!file_exists($sourcePath) || !is_file($sourcePath)) {
            $img = new sfImage();
            $img->create(200, 200, '#A8A8A8');
//      $img->text('no pic', 10, 10, 20, 'Arial', '#FF0000');
            $img->setQuality(100);
            $img->saveAs($sourcePath, 'image/jpg');
        }
    }

//    if (!file_exists($destPath)) {
//        return "http://placehold.it/{$maxWidth}x{$maxHeight}";
//    }

    if (!file_exists($destPath)) {
        $i = new sfImage($sourcePath);

        $con = min($i->getWidth(), $i->getHeight());

        if ($i->getWidth() > $i->getHeight()) {
            $left = round(($i->getWidth() - $i->getHeight()) / 2);
            $top = 0;
        } elseif ($i->getWidth() < $i->getHeight()) {
            $left = 0;
            #by vit
            #$top = $i->getHeight() / 2 - $i->getWidth();
            $top = round(($i->getHeight() - $i->getWidth()) / 2);
        } else {
            $left = $top = 0;
        }
        //crop if pic bigget than neede icon
        if ($con > min($maxWidth, $maxHeight)) {
            $i->crop($left, $top, $con, $con);
        } else {
            //die('overlay');
            //use overlay
            $i = new sfImage(sfConfig::get('sf_web_dir') . '/uploads/fake-icon.jpg');
            $i->overlay(new sfImage($sourcePath), 'middle-center'); // or you can use coords array($x,$y)

        }

        $i->thumbnail($maxWidth, $maxHeight);
        $i->setQuality(100);
        $i->saveAs($destPath, 'image/jpg');
    }

    return image_path('/uploads/thumbs/' . $thumbName);
}

function thumb_path($path, $width, $height)
{
    $path = ($path{0} == '/' ? '' : '/') . $path;

    $width = (int)$width;
    $height = (int)$height;

    $fileName = basename($path);

    $thumbsDir = sfConfig::get('sf_web_dir') . '/uploads/thumbs';
    if (!file_exists($thumbsDir)) {
        mkdir($thumbsDir);
        chmod($thumbsDir, 0777);
    }

    $thumbName = $fileName . '-' . $width . 'x' . $height . '.jpg';
    $thumbPath = $thumbsDir . '/' . $thumbName;

    $sourcePath = sfConfig::get('sf_web_dir') . '/' . $path;
    $destPath = $thumbPath;

    if (!file_exists($sourcePath) || !is_file($sourcePath)) {
        $sourcePath = sfConfig::get('sf_web_dir') . '/i/000/img100x100_02.jpg';
    }

    if (!file_exists($destPath)) {
        $i = new sfImage($sourcePath);

        $proportion = round($width / $height, 4);
        $centerX = round($i->getWidth() / 2);
        $centerY = round($i->getHeight() / 2);

        if (($i->getWidth() > $i->getHeight()) &&
            (round($i->getHeight() * $proportion >= $i->getWidth()))
        ) // landscape
        {
            $left = 0;
            $cropWidth = $i->getWidth();
            $top = $centerY - round(($cropWidth / $proportion / 2));
            $cropHeight = round($i->getWidth() / $proportion);
        } elseif ($i->getWidth() < $i->getHeight()) // portrait
        {
            $left = 0;
            $cropWidth = $i->getWidth();
            $top = $centerY - round($height / 2);
            $cropHeight = round($i->getWidth() / $proportion);
        } else {
            $top = 0;
            $cropHeight = $i->getHeight();
            $cropWidth = round($i->getHeight() * $proportion);
            $left = $centerX - round($i->getHeight() * $proportion);
        }

//    die('crop($left, $top, $cropWidth, $cropHeight) '."crop(".$i->getWidth()." != ".$i->getHeight().":: $left, $top, $cropWidth, $cropHeight)");
        $i->crop($left, $top, $cropWidth, $cropHeight);
        $i->thumbnail($width, $height);
        $i->setQuality(100);
        $i->saveAs($destPath, 'image/jpg');
    }

    return image_path('/uploads/thumbs/' . $thumbName);
}


function pic_watermarked($path, $noPic = 'no-pic.jpg', $watermark = 'watermark-50.png')
{
    $path = ($path{0} == '/' ? '' : '/') . $path;

    $fileName = basename($path);

    $thumbsDir = sfConfig::get('sf_web_dir') . '/uploads/thumbs';
    if (!file_exists($thumbsDir)) {
        mkdir($thumbsDir);
        chmod($thumbsDir, 0777);
    }

    $thumbName = $fileName . '-full.jpg';
    $thumbPath = $thumbsDir . '/' . $thumbName;

    $sourcePath = sfConfig::get('sf_web_dir') . '/' . $path;
    $destPath = $thumbPath;

    if (!file_exists($sourcePath) || !is_file($sourcePath)) {
        $sourcePath = $thumbsDir . '/' . $noPic;
        if (!file_exists($sourcePath) || !is_file($sourcePath)) {
            $img = new sfImage();
            $img->create(200, 200, '#A8A8A8');
            $img->text('no pic', 10, 10, 20, 'Arial', '#FF0000');
            $img->setQuality(100);
            $img->saveAs($sourcePath, 'image/jpg');
        }
    }
    // if thumb not exists
    if (!file_exists($destPath)) {

        if ($watermark) {
            $i = new sfImage($sourcePath);
            $i->overlay(new sfImage(sfConfig::get('sf_web_dir') . '/uploads/watermark/' . $watermark), 'top-left'); // or you can use coords array($x,$y)
            $i->saveAs($destPath, 'image/jpg');
        }

        return image_path('/uploads/thumbs/' . $thumbName);
    } // if thumb exists

    return image_path('/uploads/thumbs/' . $thumbName);
}

function thumbnail_crop($path, $maxWidth, $maxHeight, $noPic = 'no-pic.jpg', $watermark = false)
{
    $path = ($path{0} == '/' ? '' : '/') . $path;

    $fileName = basename($path);

    $thumbsDir = sfConfig::get('sf_web_dir') . '/uploads/thumbs';
    if (!file_exists($thumbsDir)) {
        mkdir($thumbsDir);
        chmod($thumbsDir, 0777);
    }

    $thumbName = $fileName . '-' . $maxWidth . 'x' . $maxHeight . '.jpg';
    $thumbPath = $thumbsDir . '/' . $thumbName;

    $sourcePath = sfConfig::get('sf_web_dir') . '/' . $path;
    $destPath = $thumbPath;

    if (!file_exists($sourcePath) || !is_file($sourcePath)) {
        $sourcePath = $thumbsDir . '/' . $noPic;
        if (!file_exists($sourcePath) || !is_file($sourcePath)) {
            $img = new sfImage();
            $img->create(200, 200, '#A8A8A8');
//      $img->text('no pic', 10, 10, 20, 'Arial', '#FF0000');
            $img->setQuality(100);
            $img->saveAs($sourcePath, 'image/jpg');
        }
    }
    // if thumb not exists
    if (!file_exists($destPath)) {
        $i = new sfImage($sourcePath);

        // $maxWidth = null, $maxHeight = null, $scale = true, $inflate = true, $quality = 75, $adapterClass = null, $adapterOptions = array()
        $thumbnail = new sfThumbnail($maxWidth, $maxHeight, $scale = false, $inflate = true, $quality = 75,
            $adapterClass = 'sfGDAdapter', // null, //
            $adapterOptions = array('method' => 'shave_all')
        );
        $thumbnail->loadFile($sourcePath);
        $thumbnail->save($destPath, $targetMime = null);

        if ($watermark) {
            $i = new sfImage($destPath);
            $i->overlay(new sfImage(sfConfig::get('sf_web_dir') . '/uploads/watermark/' . $watermark), 'top-left'); // or you can use coords array($x,$y)
            $i->saveAs($destPath, 'image/jpg');
        }

        return image_path('/uploads/thumbs/' . $thumbName);
    } // if thumb exists

    return image_path('/uploads/thumbs/' . $thumbName);
}

function thumbnail_crop_watermarked($path, $maxWidth, $maxHeight, $noPic = 'no-pic.jpg', $watermark = 'watermark-800.png')
{
    $path = ($path{0} == '/' ? '' : '/') . $path;

    $fileName = basename($path);

    $thumbsDir = sfConfig::get('sf_web_dir') . '/uploads/thumbs';
    if (!file_exists($thumbsDir)) {
        mkdir($thumbsDir);
        chmod($thumbsDir, 0777);
    }

    $thumbName = $fileName . '-' . $maxWidth . 'x' . $maxHeight . '.jpg';
    $thumbPath = $thumbsDir . '/' . $thumbName;

    $sourcePath = sfConfig::get('sf_web_dir') . '/' . $path;
    $destPath = $thumbPath;

    if (!file_exists($sourcePath) || !is_file($sourcePath)) {
        $sourcePath = $thumbsDir . '/' . $noPic;
        if (!file_exists($sourcePath) || !is_file($sourcePath)) {
            $img = new sfImage();
            $img->create(200, 200, '#A8A8A8');
            $img->setQuality(100);
            $img->saveAs($sourcePath, 'image/jpg');
        }
    }
    // if thumb not exists
    if (!file_exists($destPath)) {
        $i = new sfImage($sourcePath);

        // $maxWidth = null, $maxHeight = null, $scale = true, $inflate = true, $quality = 75, $adapterClass = null, $adapterOptions = array()
        $thumbnail = new sfThumbnail($maxWidth, $maxHeight, $scale = false, $inflate = true, $quality = 75,
            $adapterClass = 'sfImageMagickAdapter', // null, // 
            $adapterOptions = array('method' => 'shave_all')
        );
        $thumbnail->loadFile($sourcePath);
        $thumbnail->save($destPath, $targetMime = null);

        if ($watermark) {
            $i = new sfImage($destPath);
            // create watermark
            $w = new sfImage(sfConfig::get('sf_web_dir') . '/uploads/watermark/' . $watermark);

            // resize watermark depends on setting
            $width = round($i->getWidth() * sfConfig::get('app_my_gallery_watermark_size', 1));
            $w->resize($width, null);

            // check opacity
//      $opacity = sfConfig::get('app_my_gallery_watermark_opacity', 1);
//      if ($opacity < 1)
//      {
//        $w->opacity($opacity);
//      }
            // overlay w/watermark
            $i->overlay($w, sfConfig::get('app_my_gallery_watermark_position', 'bottom-right')); // or you can use coords array($x,$y)
            $i->saveAs($destPath, 'image/jpg');
        }

        return image_path('/uploads/thumbs/' . $thumbName);
    } // if thumb exists

    return image_path('/uploads/thumbs/' . $thumbName);
}

/**
 *
 * @param string $path
 * @param type $noPic
 * @param type $watermark
 * @return wetermarked picture on center by full width
 */
function pic_full_watermarked($path, $noPic = 'no-pic.jpg', $watermark = 'watermark-800.png')
{
    $path = ($path{0} == '/' ? '' : '/') . $path;

    $fileName = basename($path);

    $thumbsDir = sfConfig::get('sf_web_dir') . '/uploads/thumbs';
    if (!file_exists($thumbsDir)) {
        mkdir($thumbsDir);
        chmod($thumbsDir, 0777);
    }

    $thumbName = $fileName . '-full.jpg';
    $thumbPath = $thumbsDir . '/' . $thumbName;

    $sourcePath = sfConfig::get('sf_web_dir') . '/' . $path;
    $destPath = $thumbPath;

    if (!file_exists($sourcePath) || !is_file($sourcePath)) {
        $sourcePath = $thumbsDir . '/' . $noPic;
        if (!file_exists($sourcePath) || !is_file($sourcePath)) {
            $img = new sfImage();
            $img->create(200, 200, '#A8A8A8');
            $img->text('no pic', 10, 10, 20, 'Arial', '#FF0000');
            $img->setQuality(100);
            $img->saveAs($sourcePath, 'image/jpg');
        }
    }
    // if thumb not exists
    if (!file_exists($destPath)) {

        if ($watermark) {
            $i = new sfImage($sourcePath);
            // create wetermark
            $w = new sfImage(sfConfig::get('sf_web_dir') . '/uploads/watermark/' . $watermark);
            // resize watermark depends on setting
            $width = round($i->getWidth() * sfConfig::get('app_my_gallery_watermark_size', 1));
            $w->resize($width, null);

            // overlay w/watermark
            $i->overlay($w, sfConfig::get('app_my_gallery_watermark_position', 'bottom-right')); // or you can use coords array($x,$y)
            $i->saveAs($destPath, 'image/jpg');
        }

        return image_path('/uploads/thumbs/' . $thumbName);
    } // if thumb exists

    return image_path('/uploads/thumbs/' . $thumbName);
}

