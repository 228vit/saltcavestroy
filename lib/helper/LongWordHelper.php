<?php

function brakeLongWords($word)
{
  $maxLen = myConfig::get('мах_word_len', '17', 'Максимальная длина слова в заголовке и анонсе');

  $shy = (strpos($_SERVER["HTTP_USER_AGENT"],'Firefox')=== false)?"&shy;":"<WBR>" ;
  $len = mb_strlen($word, 'utf-8');
  $chunks = round($len/$maxLen);

  $a = array();
  for ($i = 0; $i < $chunks; $i++)
  {
    $a[] = mb_substr($word, $i*$maxLen, $maxLen, 'utf-8');
  }

  return implode($shy, $a);
}

?>
