<?php
function declension($n, $one, $some, $many)
{
  if ($n % 10 == 1 && $n % 100 != 11)
  {
    return $one;
  }
  elseif (($n % 10 > 1 && $n < 5) && ($n % 100 < 11 || $n % 100 > 14))
  {
    return $some;
  }
  else
  {
    return $many;
  }
}