<?php
function include_datepicker_js()
{
  use_javascript('date', sfWebResponse::FIRST);
  use_javascript('date_ru_utf8', sfWebResponse::FIRST);
  use_javascript('jq/jquery.datePicker.js', sfWebResponse::MIDDLE);
  use_javascript('jq/datepicker', sfWebResponse::LAST);
}

function include_datepicker_css()
{
  use_stylesheet('datePicker');
}

function render_datepickers()
{
  include_datepicker_js();
  include_datepicker_css();
}