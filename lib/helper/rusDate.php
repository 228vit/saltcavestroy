<?php
function getMysqlFormatFromRussianDateString($string)
{
  $patterns = array(
    array('!янв\s+!','!фев\s+!','!мар\s+!','!апр\s+!','!май\s+!','!июн\s+!','!июл\s+!','!авг\s+!','!сен\s+!','!окт\s+!','!ноя\s+!','!дек\s+!'),
    array('!январь\s+!','!февраль\s+!','!март\s+!','!апрель\s+!','!май\s+!','!июнь\s+!','!июль\s+!','!август\s+!','!сентябрь\s+!','!октябрь\s+!','!ноябрь\s+!','!декабрь\s+!'),
    array('!января\s+!','!февраля\s+!','!марта\s+!','!апреля\s+!','!мая\s+!','!июня\s+!','!июля\s+!','!августа\s+!','!сентября\s+!','!октября\s+!','!ноября\s+!','!декабря\s+!'),
  );

  $replacement = array(
    'january',
    'february',
    'march',
    'april',
    'may',
    'june',
    'july',
    'august',
    'september',
    'october',
    'november',
    'december'
  );

  foreach ($patterns as $pattern)
  {
    $date = preg_replace($pattern, $replacement, $string, -1, $count);

    if ($count)
    {
      return date('Y-m-d H:i:s', strtotime($date));
    }
  }

  return false;
}

#echo getMysqlFormatFromRussianDateString('28 февраля 2010');