<?php

function num2str_en($num) {
    $nul='zero';
    $ten=array(
        array('', "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"),
        array('', "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"),
    );
    $a20=array("ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen");
    $tens=array(2=>"twenty", "thirty", "fourty", "fifty", "sixty", "seventy", "eighty", "ninety");
    $unit=array( // Units
        array('kop', 'kop', 'kop', 1),
        array('rouble', 'rouble', 'rouble', 0),
        array('thousand', 'thousand', 'thousand', 1),
        array('million', 'million', 'million', 0),
        array('billion', 'billion', 'billion', 0),
    );
    //
    list($rub,$kop) = explode('.',sprintf("%015.2f", floatval($num)));
    $out = array();
    if (intval($rub)>0) {
        foreach(str_split($rub,3) as $uk=>$v) { // by 3 symbols
            if (!intval($v)) continue;
            $uk = sizeof($unit)-$uk-1; // unit key
            $gender = $unit[$uk][3];
            list($i1,$i2,$i3) = array_map('intval',str_split($v,1));
            // mega-logic
            if ($i1 != 0) $out[] = $ten[0][$i1] . ' hundred'; # 1xx-9xx
            if ($i2>1) $out[]= $tens[$i2].' '.$ten[$gender][$i3]; # 20-99
            else $out[]= $i2>0 ? $a20[$i3] : $ten[$gender][$i3]; # 10-19 | 1-9
            // units without rub & kop
            if ($uk>1) $out[]= morph_en($v,$unit[$uk][0],$unit[$uk][1],$unit[$uk][2]);
        } //foreach
    }
    else $out[] = $nul;
    $out[] = morph_en(intval($rub), $unit[1][0],$unit[1][1],$unit[1][2]); // rub
    $out[] = $kop.' '.morph_en($kop,$unit[0][0],$unit[0][1],$unit[0][2]); // kop
    return trim(preg_replace('/ {2,}/', ' ', join(' ',$out)));
}

function morph_en($n, $f1, $f2, $f5) {
    $n = abs(intval($n)) % 100;
    if ($n>10 && $n<20) return $f5;
    $n = $n % 10;
    if ($n>1 && $n<5) return $f2;
    if ($n==1) return $f1;
    return $f5;
}

function start_en($arr) {
    $arr = explode(';', $arr);
    $i = 1;
    foreach ($arr as $sum) {
        echo $i . '.  '.$sum. ' (' .num2str(trim($sum)) .')';
        print("\n");
        $i++;
    }
}