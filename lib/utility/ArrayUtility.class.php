<?php

// ARRAY FILTER RECURSIVE USING CLASS, STATIC METHOD, AND ANONYMOUS CALLBACK FUNCTION
// NOTE THAT THE CALLBACK HAS ACCESS TO BOTH THE KEY AND VALUE
// THE CLASS (FOR YOU TO COPY)
class ArrayUtility
{

    public static $result = array();

    static function GetOnlyArrayValuesRecursive($source, $result = array())
    {
        // if 1st call
        if (sizeof($result) == 0) {
            self::$result = array();
        }
        foreach ($source as $key => $value) {
            if (is_array($value) && sizeof($value)) {
                $res = self::GetOnlyArrayValuesRecursive($value, $result);
                continue;
            }

            // empty() kills "0" values
            if (trim($value) !== '') {
                self::$result[] = $value;
                $result[] = $value; // KEEP
                continue;
            }
        }

        return self::$result;
    }

    /**
     * Parce fields like %login%, %password%, '%uid%'
     *
     * @param array $source
     * @return array
     */
    static function ReplaceInArrayRecursive($source, $replaces = array('%login%', '%password%', '%uid%', '%date%'))
    {
        $result = array();
        foreach ($source as $key => $value) {
            if (is_array($value) && sizeof($value)) {
                $res = self::ReplaceInArrayRecursive($value);
                // dont use empty arrays
                if (count($res)) {
                    $result[$key] = $res;
                }
                continue;
            }

            // check if value == '%xxx%'
            if (in_array($value, $replaces)) {
                // check if method xxxReplace exists
                $method_name = str_replace('%', '', $value) . 'Replace'; // loginReplace, passwordReplace...
                if (is_callable("self::$method_name")) {
                    $replacedVal = self::$method_name();
                    if ($replacedVal) {
                        $result[$key] = $replacedVal;
                    }
                } else {
                    $result[$key] = $value; // no method exists, keep original value
                }
            } else {
                // no %xxx% found, keep original value
                $result[$key] = $value;
            }
        } // foreach

        return $result;
    }

    /**
     * Generate login
     *
     * @param int $length
     * @return string
     */
    private static function loginReplace($length = 6)
    {
        return StringUtility::generateRandomString($length);
    }

    /**
     * Generate password
     *
     * @param int $length
     * @return string
     */
    private static function passwordReplace($length = 8)
    {
        return StringUtility::generateRandomString($length);
    }

    /**
     * Generate uid
     *
     * @param int $length
     * @return string
     */
    private static function uidReplace($length = 10)
    {
        return md5(rand(1111, 9999) . time());
    }

    /**
     * Generate date
     *
     * @param int $length
     * @return string
     */
    private static function dateReplace($length = 10)
    {
        return date('Y-m-d');
    }

    /**
     * Clean array for empty values $val => '', and empty arrays - $val = Array()
     *
     * @param array $source
     * @return array
     */
    static function CleanEmptyValuesAndArraysRecursive($source)
    {
        $result = array();
        foreach ($source as $key => $value) {
            if (is_array($value) && sizeof($value)) {
                $res = self::CleanEmptyValuesAndArraysRecursive($value);
                // dont use empty arrays
                if (is_array($res) && count($res) != 0) {
                    $result[$key] = $res;
                }
                continue;
            }

            // empty() kills "0" values
            if (!is_array($value) && trim($value) !== '') {
                $result[$key] = $value; // KEEP
                continue;
            }
        }

        return $result;
    }

    /**
     * Clean array for empty values $val => ''
     *
     * @param array $source
     * @return array
     */
    static function CleanArrayRecursive($source)
    {
        $result = array();
        foreach ($source as $key => $value) {
            if (is_array($value)) {
                $result[$key] = self::CleanArrayRecursive($value);
                continue;
            }

            if (trim($value)) {
                $result[$key] = $value; // KEEP
                continue;
            } else {
                // echo "empty found $key => $value \n";
            }
        }

        return $result;
    }

}