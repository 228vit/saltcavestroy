<?php
class UpdateLogger
{
    public static function log(sfDoctrineRecord $record)
    {
        try {
            if ($record->isModified()) {

                /** @var $user sfGuardUser */
                $guard_user = sfContext::getInstance()->getUser()->getGuardUser();

                $r = new UpdateLog();
                $r->sf_guard_user_id = $guard_user->id;
                $r->model_name = get_class($record);
                $r->model_id = $record->getPrimaryKey();
                $r->description = '';

                log::info(print_r($record->getModified($old = true), true));
                $old_values = $record->getModified($old = true);
                $new_values = $record->getModified($old = false);

                $sql = "UPDATE " . $record->getTable()->getTableName() . " SET ";
                $sql_fields = [];

                foreach ($new_values as $field => $value) {
                    $sql_fields[] = "$field = '".(string)$old_values[$field]."'";
                    $r->description .= "{$field}: {$old_values[$field]} => {$value}\n";
                }
                $sql .= implode(', ', $sql_fields);
                $sql .= " WHERE id = " . $record->getPrimaryKey();

                $r->rollback_sql = $sql;

                $r->save();
            }

        } catch (Exception $e) {
            log::error($e->getCode(), $e->getMessage());
            log::error($e->getCode(), $e->getTraceAsString());
        }

    }

}
