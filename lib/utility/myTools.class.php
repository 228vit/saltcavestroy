<?php
class myTools
{

  static public function getCKEditorConfigShort()
  {
    $config=array();
    $config['toolbar_Full'] = array(
      array('Bold','Italic','Strike'),
    );

    return $config;
  }

  static public function getCKEditorConfig()
  {
    $config=array();
    $config['toolbar_Full'] = array(
      array('Source','-','Save',/*'NewPage','Preview','-','Templates'*/),
      array('Cut','Copy','Paste','PasteText','PasteFromWord','-',/*'Print', 'SpellChecker', 'Scayt'*/),
      array(/*'Undo','Redo','-','Find','Replace','-',*/'SelectAll','RemoveFormat'),
      //array('Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField'),
      //'/',
      array(/*'Styles',*/'Format','Font','FontSize'),
      array('Bold','Italic','Underline','Strike','-','Subscript','Superscript'),
      array('NumberedList','BulletedList','-','Outdent','Indent',/*'Blockquote','CreateDiv'*/),
      array('JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'),
      array('BidiLtr', 'BidiRtl'),
      array('Link','Unlink',/*'Anchor'*/),
      array('Image', 'Youtube', 'Flash','Table','HorizontalRule',/*'Smiley','SpecialChar','PageBreak','Iframe'*/),
      //'/',
      array('TextColor','BGColor'),
      array('Maximize', 'ShowBlocks'/*,'-','About'*/)
    );

//    $config['filebrowserBrowseUrl'] = '/js/ckfinder/js/ckfinder.html';
//    $config['filebrowserImageBrowseUrl'] = '/js/ckfinder/js/ckfinder.html?type=Images';
//    $config['filebrowserFlashBrowseUrl'] = '/js/ckfinder/js/ckfinder.html?type=Flash';
//    $config['filebrowserUploadUrl'] = '/js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';
//    $config['filebrowserImageUploadUrl'] = '/js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';
//    $config['filebrowserFlashUploadUrl'] = '/js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';
    $config['height'] = 400;

    return $config;
  }

  static public function getIP()
  {
    if (getenv('HTTP_CLIENT_IP') && strcasecmp(getenv('HTTP_CLIENT_IP'), 'unknown'))
      $ip = getenv('HTTP_CLIENT_IP');
    elseif (getenv('HTTP_X_FORWARDED_FOR') && strcasecmp(getenv('HTTP_X_FORWARDED_FOR'), 'unknown'))
      $ip = getenv('HTTP_X_FORWARDED_FOR');
    elseif (getenv('REMOTE_ADDR') && strcasecmp(getenv('REMOTE_ADDR'), 'unknown'))
      $ip = getenv('REMOTE_ADDR');
    elseif (!empty($_SERVER['REMOTE_ADDR']) && strcasecmp($_SERVER['REMOTE_ADDR'], 'unknown'))
      $ip = $_SERVER['REMOTE_ADDR'];
    else
      $ip = 'unknown';
    return ($ip);
  }

  static public function getSizeName($size = 0, $postfix_id = 0)
  {
    $size_postfix_names = array('байт', 'Кб', 'Мб', 'Гб', 'Тб');
    if ($size < 1024)
      return $size . ' ' . $size_postfix_names[$postfix_id];
    $postfix_id++;
    return self::getSizeName(round($size / 1024, 1), $postfix_id);
  }

  static public function generateHash($length = 8)
  {
    $hash = '';

    for ($i = 0; ($i < $length); $i++)
      $hash .= sprintf('%02x', mt_rand(0, 255));

    return $hash;
  }

}
