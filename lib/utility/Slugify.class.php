<?php

class SlugifyClass
{
    static function Slugify($title)
    {
        $gost = array(
            "Є" => "EH", "І" => "I", "і" => "i", "№" => "#", "є" => "eh",
            "А" => "A", "Б" => "B", "В" => "V", "Г" => "G", "Д" => "D",
            "Е" => "E", "Ё" => "JO", "Ж" => "ZH",
            "З" => "Z", "И" => "I", "Й" => "JJ", "К" => "K", "Л" => "L",
            "М" => "M", "Н" => "N", "О" => "O", "П" => "P", "Р" => "R",
            "С" => "S", "Т" => "T", "У" => "U", "Ф" => "F", "Х" => "KH",
            "Ц" => "C", "Ч" => "CH", "Ш" => "SH", "Щ" => "SHH", "Ъ" => "'",
            "Ы" => "Y", "Ь" => "", "Э" => "EH", "Ю" => "YU", "Я" => "YA",
            "а" => "a", "б" => "b", "в" => "v", "г" => "g", "д" => "d",
            "е" => "e", "ё" => "jo", "ж" => "zh",
            "з" => "z", "и" => "i", "й" => "jj", "к" => "k", "л" => "l",
            "м" => "m", "н" => "n", "о" => "o", "п" => "p", "р" => "r",
            "с" => "s", "т" => "t", "у" => "u", "ф" => "f", "х" => "kh",
            "ц" => "c", "ч" => "ch", "ш" => "sh", "щ" => "shh", "ъ" => "",
            "ы" => "y", "ь" => "", "э" => "eh", "ю" => "yu", "я" => "ya", "«" => "", "»" => "", "—" => "-", " " => "_"
        );

        $iso = array(
            "Є" => "YE", "І" => "I", "Ѓ" => "G", "і" => "i", "№" => "#", "є" => "ye", "ѓ" => "g",
            "А" => "A", "Б" => "B", "В" => "V", "Г" => "G", "Д" => "D",
            "Е" => "E", "Ё" => "YO", "Ж" => "ZH",
            "З" => "Z", "И" => "I", "Й" => "J", "К" => "K", "Л" => "L",
            "М" => "M", "Н" => "N", "О" => "O", "П" => "P", "Р" => "R",
            "С" => "S", "Т" => "T", "У" => "U", "Ф" => "F", "Х" => "X",
            "Ц" => "C", "Ч" => "CH", "Ш" => "SH", "Щ" => "SHH", "Ъ" => "'",
            "Ы" => "Y", "Ь" => "", "Э" => "E", "Ю" => "YU", "Я" => "YA",
            "а" => "a", "б" => "b", "в" => "v", "г" => "g", "д" => "d",
            "е" => "e", "ё" => "yo", "ж" => "zh",
            "з" => "z", "и" => "i", "й" => "j", "к" => "k", "л" => "l",
            "м" => "m", "н" => "n", "о" => "o", "п" => "p", "р" => "r",
            "с" => "s", "т" => "t", "у" => "u", "ф" => "f", "х" => "x",
            "ц" => "c", "ч" => "ch", "ш" => "sh", "щ" => "shh", "ъ" => "",
            "ы" => "y", "ь" => "", "э" => "e", "ю" => "yu", "я" => "ya", "«" => "", "»" => "", "—" => "-", " " => "_"
        );

        $rtl_standard = sfConfig::get('rtl_standard', 'gost');

        //to lower case
        $title = mb_strtolower($title, 'UTF-8');

        switch ($rtl_standard) {
            case 'off':
                return $title;
            case 'gost':
                $out = trim(strtr($title, $gost), '_');
            default:
                $out = trim(strtr($title, $iso), '_');
        }

        // clean slug
        $out = preg_replace('/[^a-z0-9\-\_]+/i', '', $out);
        return $out;
    }

    /**
     * Input value 2015-10-06 11:09:00 1 (date time id)
     *
     * @param $value
     * @return mixed
     */
    static function SlugifyDigitsOnly($value)
    {
        $value = mb_strtolower($value, 'UTF-8');

        $value_items = explode(' ', $value);

        if (count($value_items) == 1) {
            return preg_replace('/[^0-9]{1,}/i', '', $value);
        }

        $value = $value_items[0]; // kill time

        // clean slug, left only digits, then return
        $value = preg_replace('/[^0-9]+/i', '', $value);
//        print_r($value_items);
//        die($value);

        return $value;
    }


}
