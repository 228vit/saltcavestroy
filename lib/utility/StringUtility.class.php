<?php

/**
 * and open the template in the editor.
 */
class StringUtility {
 /**
  * Generate random string for login/password
  * 
  * @param int $length
  */
  public static function generateRandomString($length=10)
  {
    return substr(str_shuffle("2345679abcdefhijkmnprstuvwxyzACDEFGHJKLMNPQRSTUVWXYZ"), 0, $length);
  }
}
?>
