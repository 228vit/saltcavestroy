<?php
class UserUtils
{

    public static $roles = array(
        'finance' => 'финансы',
        'technic' => 'техник',
        'technical' => 'техник',
        'admin' => 'администратор'
    );

    public static function getRoleRus($role)
    {

        if (isSet(self::$roles[$role])) {
            return self::$roles[$role];
        } else {
            return $role;
        }
    }

}