<?php

/**
 * Log anything
 *
 * @author vit
 */
class log
{

    /**
     * Wrapper method for getting a symfony logger object
     * // return sfContext::getInstance()->getLogger();
     * @return object
     */
    public static function getLogger($file_name = 'info.log')
    {
        $log_path = sfConfig::get('sf_log_dir') . DIRECTORY_SEPARATOR . $file_name;
        $logger = new sfFileLogger(new sfEventDispatcher(), array('file' => $log_path));

        return $logger;
    }

    /**
     * Wrapper method for logging info message
     *
     * @param string $message
     */
    public static function info($message, $file_name = 'info.log')
    {
        $fa = explode('.log', $file_name);
        $fa[0] = isSet($fa[0]) ? $fa[0] : 'info';
        $file_name = $fa[0] . '_' . date("Y-m-d") . '.log';
        self::getLogger($file_name)->info($message);
    }

    /**
     * Wrapper method for logging debug message
     *
     * @param string $message
     */
    public static function debug($message, $file_name = 'debug.log')
    {
        $fa = explode('.log', $file_name);
        $fa[0] = isSet($fa[0]) ? $fa[0] : 'debug';
        $file_name = $fa[0] . '_' . date("Y-m-d") . '.log';
        self::getLogger($file_name)->debug($message);
    }

    /**
     * Wrapper method for logging an error
     *
     * @param string $code
     * @param string $message
     */
    public static function error($code, $message, $file_name = 'error.log')
    {
        $fa = explode('.log', $file_name);
        $fa[0] = isSet($fa[0]) ? $fa[0] : 'error';
        $file_name = $fa[0] . '_' . date("Y-m-d") . '.log';

        self::getLogger($file_name)->err("code: {$code}, message: " . $message);
    }

    public static function addToRegister($message, $bankName, $productOutId, $date)
    {
        $fileName = $bankName . '_' . $productOutId . '_' . $date . '_register' . '.csv';

        $csv_path = sfConfig::get('sf_log_dir') . DIRECTORY_SEPARATOR . 'register' . DIRECTORY_SEPARATOR . $fileName;
        $logger = new sfFileLogger(new sfEventDispatcher(), array('file' => $csv_path,
            'format' => '%message%%EOL%'));
        $logger->info($message);
    }
}
