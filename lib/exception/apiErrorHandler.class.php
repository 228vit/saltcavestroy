<?php

class pbErrorHandler
{

    /**
     * Catch all uncatched exceptions
     *
     * @param Exception $e
     */
    public static function handleError($e)
    {
        $data = array(
            'status' => 'fail',
            'error' => [
                'message' => $e->getMessage(),
                'code' => $e->getCode(),
            ],
        );

        // log if uncatched and unlogged Exception
        if ($e instanceof pbException && is_callable([get_class($e), 'getType'])) {
            $data['error']['type'] = $e->getType();
            $json = json_encode($data);
        } else {
            $json = json_encode($data);
            // log to apache error.log
            error_log($json);
        }

        header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
        header('Content-type: application/json');
        die($json);
    }

}