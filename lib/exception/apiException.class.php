<?php
/**
 * API exception
 */
class apiException extends Exception
{

    const Err = 100;
    const OK = 200;
    const BadRequest = 400;
    const AuthErr = 401;
    const NotFound = 404;
    const ServerError = 500;

    private $_messages = [
        0 => 'Internal unexpected error',
        100 => 'Error not described',
        200 => 'Successful operation',
        400 => 'Bad request format',
        401 => 'Authorisation error',
        404 => 'Resource not found',
        500 => 'Internal server error',
    ];
    protected $message;
    private $string;
    protected $code;
    protected $file;
    protected $line;
    private $trace;
    private $previous;


    /**
     * Exception controller, if no error code, it will try guess code by message
     *
     * @param string $message
     * @param mixed $code
     * @param type $previous
     */
    public function __construct($message, $code, $previous = FALSE)
    {
        $this->code = $code;
        $this->message = isSet($this->_messages[$code]) ? $this->_messages[$code] : $message;
    }

    /**
     *  {"error":{"message":"Invalid OAuth access token.","type":"OAuthException","code":190}}
     * @return string
     */
    public function getType()
    {
        return 'platboxException';
    }

    /**
     * (PHP 5 >= 5.1.0)
     * String representation of the exception
     * @link http://php.net/manual/en/exception.tostring.php
     * @return string the string representation of the exception.
     */
    public function __toString()
    {
        return $this->getCode() . ' ' . $this->getMessage();
    }

}