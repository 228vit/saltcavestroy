<?php

class apiAffiliateException extends pbException
{

    /**
     * @var array $_errorMessages       an array of error messages
     */
    private $_messagesEng = array(
        100 => 'Unknown error',
        101 => 'Wrong API key',
        102 => 'Wrong signature',
        103 => 'Wrong money source',
        104 => 'Wrong amount',
        105 => 'Wrong request form',
        106 => 'Wrong success URL',
        107 => 'Wrong fail URL',
        108 => 'Wrong transaction ID',
        109 => 'Wrong additional information',
        110 => 'Wrong comment information',
        111 => 'Wrong phone number',
        112 => 'Wrong POST array',
        113 => 'Temporary technical error',
        114 => 'This operator is not supported',
        115 => 'Transaction not found',
        400 => 'Incomplete data',
        404 => 'Resource not found',
        500 => 'internal server error',
    );

    public function __construct($message, $code)
    {
        // log counter
        myFileCounter::addMerchantError();
        $error = ErrorMapper::getMerchantErrorFromPlatbox($code, $message);
        $this->code = $error['error_code'];
        $this->message = $error['error_description'];
    }


    public function getType()
    {
        return 'merchantException';
    }

}