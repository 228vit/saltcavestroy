<?php

class migrationGetVersonTask extends sfDoctrineBaseTask
{

    protected function configure()
    {
        // // add your own arguments here

        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'admin'),
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'prod'),
            new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
            // add your own options here
            new sfCommandOption('ver', null, sfCommandOption::PARAMETER_REQUIRED, 'ver', 0),
        ));

        $this->namespace = 'migration';
        $this->name = 'get-ver';
        $this->briefDescription = 'Set exact migration version';
        $this->detailedDescription = <<<EOF
The [migration|INFO] Export migrations to Neyron System.
Call it with:

  [php symfony migration|INFO]
EOF;
    }

    protected function execute($arguments = array(), $options = array())
    {
        // initialize the database connection
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection = $databaseManager->getDatabase($options['connection'])->getConnection();

        $config = $this->getCliConfig();
        $migration = new Doctrine_Migration($config['migrations_path']);
        
        echo 'current version on DB: '.$migration->getCurrentVersion()."\n";
        echo 'latest version on disk: '.$migration->getLatestVersion()."\n";
    }

}
