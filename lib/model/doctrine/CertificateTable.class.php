<?php

/**
 * CertificateTable
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class CertificateTable extends Doctrine_Table
{
    /**
     * Returns an instance of this class.
     *
     * @return CertificateTable The table instance
     */
    public static function getInstance()
    {
        return Doctrine_Core::getTable('Certificate');
    }
}