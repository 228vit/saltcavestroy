<?php

/**
 * myWidgetFormDoctrineChoicePlainText represents a linked record as plain text in form
 *
 * @package    symfony
 * @subpackage doctrine
 * @author     228vit
 * @version    SVN: $Id: sfWidgetFormDoctrineChoice.class.php 29679 2010-05-30 14:46:03Z Kris.Wallsmith $
 */
class myWidgetFormDoctrineChoicePlainText extends sfWidgetFormInput
{

  /**
   * Constructor.
   *
   * Available options:
   *
   *  * model:        The model class (required)
   *  * empty_string: default = '---'
   *  * add_empty:    Whether to add a first empty value or not (false by default)
   *                  If the option is not a Boolean, the value will be used as the text value
   *  * method:       The method to use to display object values (__toString by default)
   *  * key_method:   The method to use to display the object keys (getPrimaryKey by default)
   *                    * The column to order by the results (must be in the PhpName format)
   *                    * asc or desc
   *  * query:        A query to use when retrieving objects
   *  * tag:          span by default
   *
   * @see sfWidgetFormSelect
   */
  protected function configure($options = array(), $attributes = array())
  {
    $this->addRequiredOption('model');
    $this->addOption('add_empty', false);
    $this->addOption('empty_string', '---');
    $this->addOption('method', '__toString');
    $this->addOption('key_method', 'getPrimaryKey');
    $this->addOption('tag', 'span');
    $this->addOption('query', null);

    parent::configure($options, $attributes);
  }
  
  /**
   * Renders the widget.
   *
   * @param  string $name        The element name
   * @param  string $value       The value selected in this widget
   * @param  array  $attributes  An array of HTML attributes to be merged with the default HTML attributes
   * @param  array  $errors      An array of errors for the field
   *
   * @return string An HTML tag string
   *
   * @see sfWidgetForm
   */
  public function render($name, $value = null, $attributes = array(), $errors = array())
  {
    // check if $value exists in model_table
    $object = Doctrine_Core::getTable($this->getOption('model'))->find($value);
    $method = $this->getOption('method');
    // return $object->__toString() or '---'
    return 
      $this->renderTag('input', array_merge($attributes, array('type' => 'hidden', 'name' => $name, 'value' => $value)))
      .' '.
      $this->renderHtmlTag($this->getOption('tag'), ($object ? $object->$method() : $this->getOption('empty_string')), $this->getAttributes());
  }
  /**
   * Renders a HTML tag.
   *
   * @param string $tag         The tag name
   * @param array  $attributes  An array of HTML attributes to be merged with the default HTML attributes
   *
   * @param string An HTML tag string
   */
  public function renderHtmlTag($tag, $value, $tag_attributes = array())
  {
    if (empty($tag))
    {
      return '';
    }

    return sprintf('<%s%s>%s</%s>', $tag, $this->attributesToHtml($tag_attributes), $value, $tag);
  }  

}