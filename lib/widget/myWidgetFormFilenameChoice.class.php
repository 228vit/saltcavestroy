<?php
class myWidgetFormFilenameChoice extends sfWidgetFormChoice
{
    public function __construct($options = [], $attributes = [])
    {
        $this->addRequiredOption('path');
        $this->addRequiredOption('extensions');
        $options['choices'] = [];
        parent::__construct($options, $attributes);
    }

    public function getChoices()
    {
//        die($this->options['path']);
        $choices[] = '';
        if (is_dir($this->options['path']) && is_readable($this->options['path'])) {

            try {
                // Создаем новый объект DirectoryIterator
                $dir = new DirectoryIterator($this->options['path']);

                // Цикл по содержанию директории
                foreach ($dir as $index => $file) {
                    if (is_file($this->options['path'].'/'.$file->getFilename())) {
                        $choices["foo_{$index}"] = $file->getFilename();
                    }
                }
            } catch (Exception $e) {
                $choices[] = 'dir is empty';
                log::error($e->getCode(), $e->getMessage());
                log::error($e->getCode(), $e->getTraceAsString());
            }

            // сорировка сбрасывает индексы массиву
            sort($choices);
            // key = value
            $choices = array_combine(array_values($choices), $choices);

        } else {
            $choices[] = 'dir is empty';
        }
        return $choices;
    }
}

