<?php
class myWidgetFormFilenameChoiceIterator extends sfWidgetFormChoice
{
    public function __construct($options = [], $attributes = [])
    {
        $this->addRequiredOption('path');
        $options['choices'] = [];
        parent::__construct($options, $attributes);
    }

    public function getChoices()
    {
        $choices = [];
        if(is_dir($this->options['path']) && is_readable($this->options['path']))
        {
            $d = new FileExtensionFilter(new DirectoryIterator($this->options['path']));
            foreach($d->getInnerIterator()->getBaseName() as $filename)
            {
                $choices[ucfirst($filename)] = ucfirst($filename);
            }
        }
        return $choices;
    }
}
