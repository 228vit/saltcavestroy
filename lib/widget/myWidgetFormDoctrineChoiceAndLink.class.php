<?php

/*
 * This file is part of the symfony package.
 * (c) Fabien Potencier <fabien.potencier@symfony-project.com>
 * (c) Jonathan H. Wage <jonwage@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * sfWidgetFormDoctrineChoice represents a choice widget for a model.
 *
 * @package    symfony
 * @subpackage doctrine
 * @author     Fabien Potencier <fabien.potencier@symfony-project.com>
 * @author     Jonathan H. Wage <jonwage@gmail.com>
 * @version    SVN: $Id: sfWidgetFormDoctrineChoice.class.php 29679 2010-05-30 14:46:03Z Kris.Wallsmith $
 */
class myWidgetFormDoctrineChoiceAndLink extends sfWidgetFormDoctrineChoice
{
    /**
     * @see sfWidget
     */
    public function __construct($options = array(), $attributes = array())
    {
        $options['choices'] = array();

        parent::__construct($options, $attributes);
    }

    /**
     * Constructor.
     *
     * Available options:
     *
     *  * model:        The model class (required)
     *  * add_empty:    Whether to add a first empty value or not (false by default)
     *                  If the option is not a Boolean, the value will be used as the text value
     *  * method:       The method to use to display object values (__toString by default)
     *  * key_method:   The method to use to display the object keys (getPrimaryKey by default)
     *  * order_by:     An array composed of two fields:
     *                    * The column to order by the results (must be in the PhpName format)
     *                    * asc or desc
     *  * query:        A query to use when retrieving objects
     *  * multiple:     true if the select tag must allow multiple selections
     *  * table_method: A method to return either a query, collection or single object
     *
     * @see sfWidgetFormSelect
     */
    protected function configure($options = array(), $attributes = array())
    {
        $this->addRequiredOption('model');
        $this->addOption('add_empty', false);
        $this->addOption('method', '__toString');
        $this->addOption('key_method', 'getPrimaryKey');
        $this->addOption('order_by', null);
        $this->addOption('query', null);
        $this->addOption('multiple', false);
        $this->addOption('table_method', null);
        $this->addOption('app_script_name', 'index.php');
        $this->addOption('route', null);
        $this->addOption('parent', null);

        parent::configure($options, $attributes);
    }


    /**
     * Renders the widget.
     *
     * @param  string $name        The element name
     * @param  string $value       The value selected in this widget
     * @param  array $attributes  An array of HTML attributes to be merged with the default HTML attributes
     * @param  array $errors      An array of errors for the field
     *
     * @return string An HTML tag string
     *
     * @see sfWidgetForm
     */
    public function render($name, $value = null, $attributes = array(), $errors = array())
    {
        if ($this->getOption('multiple')) {
            $attributes['multiple'] = 'multiple';

            if ('[]' != substr($name, -2)) {
                $name .= '[]';
            }
        }

        if (!$this->getOption('renderer') && !$this->getOption('renderer_class') && $this->getOption('expanded')) {
            unset($attributes['multiple']);
        }

        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));

        if ($this->getOption('route')) {
            $url = url_for('@'.$this->getOption('route').'?id='.$value);
        } else {
            $url = '';
        }

        $listbox = $this->getRenderer()->render($name, $value, $attributes, $errors);
        $link = "<a class='popupSelectedRecord' rel='" . $this->getOption('parent') . "' url='/" . $this->getOption('app_script_name') . "/" . $this->getOption('route') . "' href='#'>view</a>";
        $popup_html_id = $this->generateId('popup_' . $name);

        $script = "

        ";
//        $popup = "
//            <!-- Modal -->
//            <div class='modal fade' id='{$popup_html_id}' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>
//                <div class='modal-dialog'>
//                    <div class='modal-content'>
//                        <div class='modal-header'>
//                            <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
//                            <h4 class='modal-title' id='myModalLabel'>Modal window</h4>
//                        </div>
//                        <div class='modal-body'>
//                            ...
//                        </div>
//                        <div class='modal-footer'>
//                            <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
//                        </div>
//                    </div>
//                    <!-- /.modal-content -->
//                </div>
//                <!-- /.modal-dialog -->
//            </div><!-- /.modal -->
//        ";

        return $listbox . $link . $script ;
    }


    /**
     * Returns the choices associated to the model.
     *
     * @return array An array of choices
     */
    public function getChoices()
    {
        $choices = array();
        if (false !== $this->getOption('add_empty')) {
            $choices[''] = true === $this->getOption('add_empty') ? '' : $this->translate($this->getOption('add_empty'));
        }

        if (null === $this->getOption('table_method')) {
            $query = null === $this->getOption('query') ? Doctrine_Core::getTable($this->getOption('model'))->createQuery() : $this->getOption('query');
            if ($order = $this->getOption('order_by')) {
                $query->addOrderBy($order[0] . ' ' . $order[1]);
            }
            $objects = $query->execute();
        } else {
            $tableMethod = $this->getOption('table_method');
            $results = Doctrine_Core::getTable($this->getOption('model'))->$tableMethod();

            if ($results instanceof Doctrine_Query) {
                $objects = $results->execute();
            } else if ($results instanceof Doctrine_Collection) {
                $objects = $results;
            } else if ($results instanceof Doctrine_Record) {
                $objects = new Doctrine_Collection($this->getOption('model'));
                $objects[] = $results;
            } else {
                $objects = array();
            }
        }

        $method = $this->getOption('method');
        $keyMethod = $this->getOption('key_method');

        foreach ($objects as $object) {
            $choices[$object->$keyMethod()] = $object->$method();
        }

        return $choices;
    }
}