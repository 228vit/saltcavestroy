<?php
class myWidgetFormText extends sfWidgetForm
{
    public function configure($options = [], $attributes = [])
    {

    }

    public function render($name, $model = null, $id = null, $attributes = array(), $errors = array())
    {
        $model = $model . 'Table';
        $value = $model::getInstanse()->find($id);

        return
            $this->renderTag('input', array_merge($attributes, array('type' => 'hidden', 'name' => $name, 'value' => $id))) .
            $value;
    }
}