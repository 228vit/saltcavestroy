<?php

class myWidgetFormDoctrineChoiceNestedSet extends sfWidgetFormDoctrineChoice
{

    public function __construct($options = array(), $attributes = array())
    {
        $options['choices'] = array();

        parent::__construct($options, $attributes);
    }

    protected function configure($options = array(), $attributes = array())
    {
        $this->addRequiredOption('add_root');
        parent::configure($options, $attributes);
    }

    public function getChoices()
    {
        $choices = array();
        if (false !== $this->getOption('add_empty')) {
            $choices[''] = true === $this->getOption('add_empty') ? '' : $this->getOption('add_empty');
        }

        if (false !== $this->getOption('add_root')) {
            $choices[''] = true === $this->getOption('add_root') ? 'root' : $this->getOption('add_root');
        }

        if (null === $this->getOption('table_method')) {
            $query = null === $this->getOption('query') ? Doctrine_Core::getTable($this->getOption('model'))->createQuery() : $this->getOption('query');
            $query->addOrderBy('root_id asc')
                ->addOrderBy('lft asc');
            $objects = $query->execute();
        } else {
            $tableMethod = $this->getOption('table_method');
            $results = Doctrine_Core::getTable($this->getOption('model'))->$tableMethod();

            if ($results instanceof Doctrine_Query) {
                $objects = $results->execute();
            } else if ($results instanceof Doctrine_Collection) {
                $objects = $results;
            } else if ($results instanceof Doctrine_Record) {
                $objects = new Doctrine_Collection($this->getOption('model'));
                $objects[] = $results;
            } else {
                $objects = array();
            }
        }

        $method = $this->getOption('method');
        $keyMethod = $this->getOption('key_method');

        foreach ($objects as $object) {
            $choices[$object->$keyMethod()] = str_repeat('&nbsp;', ($object['level'] * 4)) . $object->$method();
        }

        return $choices;
    }

    public function getRenderer()
    {
        if ($this->getOption('renderer')) {
            return $this->getOption('renderer');
        }

        if (!$class = $this->getOption('renderer_class')) {
            $type = !$this->getOption('expanded') ? '' : ($this->getOption('multiple') ? 'checkbox' : 'radio');
            $class = sprintf('sfWidgetFormSelect%s', ucfirst($type));
        }

        return new $class(array_merge(array('choices' => new sfCallable(array($this, 'getChoices'))), $this->options['renderer_options']), $this->getAttributes());
    }

    /**
     * @param  string $name The element name
     * @param  string $value The value selected in this widget
     * @param  array $attributes An array of HTML attributes to be merged with the default HTML attributes
     * @param  array $errors An array of errors for the field
     *
     * @return string An HTML tag string
     *
     * @see sfWidgetForm
     */
    public function render($name, $value = null, $attributes = array(), $errors = array())
    {

        if ($this->getOption('multiple')) {
            $attributes['multiple'] = 'multiple';

            if ('[]' != substr($name, -2)) {
                $name .= '[]';
            }
        }

        if (!$this->getOption('renderer') && !$this->getOption('renderer_class') && $this->getOption('expanded')) {
            unset($attributes['multiple']);
        }

        $method_attrs = $attributes;
        $method_attrs['size'] = '4';
        $operations = new sfWidgetFormChoice(
            $options = array('choices' => array(
                'LastChildOf' => __('Last child of'),
                'FirstChildOf' => __('First child of'),
                'NextSiblingOf' => __('Move next to'),
//              'ParentOf'      => __('Make parent of'),
            )),
            $method_attrs
        );
        return $this->getRenderer()->render($name . '[parent_id]', $value, $attributes, $errors) . ' ' . $operations->render($name . '[method]', $value, $method_attrs);
    }

}
