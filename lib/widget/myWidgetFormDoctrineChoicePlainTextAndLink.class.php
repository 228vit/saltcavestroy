<?php

/*
 * This file is part of the symfony package.
 * (c) Fabien Potencier <fabien.potencier@symfony-project.com>
 * (c) Jonathan H. Wage <jonwage@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * sfWidgetFormDoctrineChoice represents a choice widget for a model.
 *
 * @package    symfony
 * @subpackage doctrine
 * @author     Fabien Potencier <fabien.potencier@symfony-project.com>
 * @author     Jonathan H. Wage <jonwage@gmail.com>
 * @version    SVN: $Id: sfWidgetFormDoctrineChoice.class.php 29679 2010-05-30 14:46:03Z Kris.Wallsmith $
 */
class myWidgetFormDoctrineChoicePlainTextAndLink extends sfWidgetFormInput
{

    /**
     * Constructor.
     *
     * Available options:
     *
     *  * model:        The model class (required)
     *  * add_empty:    Whether to add a first empty value or not (false by default)
     *                  If the option is not a Boolean, the value will be used as the text value
     *  * method:       The method to use to display object values (__toString by default)
     *  * key_method:   The method to use to display the object keys (getPrimaryKey by default)
     *  * order_by:     An array composed of two fields:
     *                    * The column to order by the results (must be in the PhpName format)
     *                    * asc or desc
     *  * query:        A query to use when retrieving objects
     *  * multiple:     true if the select tag must allow multiple selections
     *  * table_method: A method to return either a query, collection or single object
     *
     * @see sfWidgetFormSelect
     */
    protected function configure($options = array(), $attributes = array())
    {
        $this->addRequiredOption('model');
        $this->addOption('add_empty', false);
        $this->addOption('empty_string', '---');
        $this->addOption('method', '__toString');
        $this->addOption('key_method', 'getPrimaryKey');
        $this->addOption('tag', 'span');
        $this->addOption('query', null);
        $this->addOption('route', null);
        $this->addOption('parent', null);

        parent::configure($options, $attributes);
    }


    /**
     * Renders the widget.
     *
     * @param  string $name        The element name
     * @param  string $value       The value selected in this widget
     * @param  array $attributes  An array of HTML attributes to be merged with the default HTML attributes
     * @param  array $errors      An array of errors for the field
     *
     * @return string An HTML tag string
     *
     * @see sfWidgetForm
     */
    public function render($name, $value = null, $attributes = array(), $errors = array())
    {
        // check if $value exists in model_table
        $object = Doctrine_Query::create()
            ->from($this->getOption('model'))
            ->where('id = ?', $value)
            ->fetchOne();

        $method = $this->getOption('method'); // return $object->__toString() or '---'

        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));

        if ($this->getOption('route')) {
            $url = url_for('@'.$this->getOption('route').'?id='.$value);
        } else {
            $url = '';
        }

        $text = $this->renderTag('input', array_merge($attributes, array('type' => 'hidden', 'name' => $name, 'value' => $value)))
            . "<a class='ajax' href='".$url."'>".
        $this->renderHtmlTag($this->getOption('tag'), ($object ? $object->$method() : $this->getOption('empty_string')), $this->getAttributes())."</a>";

        return $text;
    }


    /**
     * Renders a HTML tag.
     *
     * @param string $tag         The tag name
     * @param array $attributes  An array of HTML attributes to be merged with the default HTML attributes
     *
     * @param string An HTML tag string
     */
    public function renderHtmlTag($tag, $value, $tag_attributes = array())
    {
        if (empty($tag)) {
            return '';
        }

        return sprintf('<%s%s>%s</%s>', $tag, $this->attributesToHtml($tag_attributes), $value, $tag);
    }
}