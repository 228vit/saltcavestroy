<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class Version2 extends Doctrine_Migration_Base
{
    public function up()
    {
        $this->removeColumn('slider', 'url');
        $this->removeColumn('slider', 'caption_style');
        $this->changeColumn('slider', 'caption', 'clob', '', array(
             ));
    }

    public function down()
    {
        $this->addColumn('slider', 'url', 'string', '255', array(
             'notnull' => '1',
             'default' => '#',
             ));
        $this->addColumn('slider', 'caption_style', 'string', '255', array(
             ));
    }
}