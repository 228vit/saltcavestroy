<?php

/**
 * article actions.
 *
 * @package    accent
 * @subpackage article
 * @author     Your name here
 * @version    SVN: $Id$
 */
class articleActions extends sfActions
{
    /**
     * Executes index action
     *
     * @param sfRequest $request A request object
     */

    public function executeIndex(sfWebRequest $request)
    {
        $this->articles = Doctrine_Core::getTable('Article')->findByIsActive(true);
//        die("[[".count($this->articles));
    }

    public function executeView(sfWebRequest $request)
    {
        $this->article = Doctrine_Core::getTable('Article')->findOneBySlug(
            $request->getParameter('slug', 'about')
        );
        $this->forward404Unless($this->article);

        $this->getResponse()->setTitle($this->article->name . ' / ' . myConfig::get('company_title', 'Студия Акцент'));

    }

    public function executeAbout(sfWebRequest $request)
    {
        $this->article = Doctrine_Core::getTable('Article')->findOneBySlug('about');
        $this->forward404Unless($this->article);

        $this->getResponse()->setTitle($this->article->title . ' / ' . myConfig::get('company_title', 'Студия Акцент'));

    }
}
