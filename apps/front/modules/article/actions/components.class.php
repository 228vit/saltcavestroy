<?php

class articleComponents extends sfComponents
{
    public function executeGetArticleBySlug(sfWebRequest $request)
    {
        $this->getArticle($this->slug);
    }

    public function executeGetRawArticleBySlug(sfWebRequest $request)
    {
        $this->getArticle($this->slug);
    }

    public function executeGetSimpleArticleBySlug(sfWebRequest $request)
    {
        $this->getArticle($this->slug);
    }

    public function executeGetWowArticleBySlug(sfWebRequest $request)
    {
        $this->getArticle($this->slug);
    }

    private function getArticle($slug)
    {
        $this->article = Doctrine_Query::create()
            ->from('Article p')
            ->where('p.slug = ?', $slug)
            ->fetchOne()
        ;
    }

    public function executeGetProductCategories(sfWebRequest $request)
    {

        $this->product_categories = Doctrine_Query::create()
            ->from('ProductCategory pc')
            ->orderBy('pc.position ASC')
            ->execute();

    }

}