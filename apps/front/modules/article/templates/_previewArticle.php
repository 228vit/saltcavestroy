<?php if ($article): ?>
    <div class="section-header">
        <h2 class="section-title text-center wow fadeInDown animated"><?php echo $article->getName(ESC_RAW) ?></h2>
    </div>
    <div class="row">
        <p>
            <?php echo $article->getAnnounce(ESC_RAW) ?>
        </p>
        <h4>
            <a href="<?php echo url_for('@article_view?slug='.$article->getSlug()) ?>">подробнее &raquo;</a>
        </h4>
    </div>
<?php else: ?>
    <p>---</p>
<?php endif; ?>
