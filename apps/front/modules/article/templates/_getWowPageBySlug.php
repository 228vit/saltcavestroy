<?php if ($article): ?>
    <h2 class="section-title text-center wow fadeInDown"><?php echo $article->getName(ESC_RAW) ?></h2>
    <p class="text-center wow fadeInDown">
        <?php echo $article->getContent(ESC_RAW) ?>
    </p>
<?php else: ?>
    <p>---</p>
<?php endif; ?>
