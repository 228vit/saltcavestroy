<?php if ($article): ?>
    <div class="section-header">
        <h2><?php echo $article->getName(ESC_RAW) ?></h2>
    </div>
    <p>
        <?php echo $article->getContent(ESC_RAW) ?>
    </p>
<?php else: ?>
    <p>---</p>
<?php endif; ?>
