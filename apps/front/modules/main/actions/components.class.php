<?php

class mainComponents extends sfComponents
{
    public function executeGetFotos(sfWebRequest $request)
    {
        $this->items = Doctrine_Query::create()
            ->from('Foto c')
            ->where('c.is_active = ?', true)
            ->orderBy('c.position ASC')
            ->execute();
    }

    public function executeFeedbackForm(sfWebRequest $request)
    {
        $this->form = new FeedbackForm();
    }

    public function executeGetCertificates(sfWebRequest $request)
    {
        $this->items = Doctrine_Query::create()
            ->from('Certificate c')
            ->where('c.is_active = ?', true)
            ->orderBy('c.position ASC')
            ->execute();
    }

    public function executeGetServicesMenuFooter(sfWebRequest $request)
    {
        $this->parent = Doctrine_Query::create()
            ->from('Menu m')
            ->where('m.slug = ?', 'uslugi')
            ->fetchOne();

        if ($this->parent) {
            $this->menu_items = Doctrine_Query::create()
                ->from('Menu m')
                ->where('m.root_id = ?', $this->parent->root_id)
                ->andWhere('m.lft > ?', $this->parent->lft)
                ->andWhere('m.rgt < ?', $this->parent->rgt)
                ->orderBy('m.root_id ASC, m.lft ASC')
                ->execute();
        }

    } // get menu

    public function executeGetMainSlider(sfWebRequest $request)
    {
        $this->sliders = Doctrine_Query::create()
            ->from('Slider s')
            ->where('s.is_active = ?', true)
            ->andWhere('s.pic <> ?', '')
            ->orderBy('s.id ASC')
            ->execute();
    } // get sliders

    public function executeGetClientsSlider(sfWebRequest $request)
    {
        $this->clients = Doctrine_Query::create()
            ->from('Client c')
            ->where('c.is_active = ?', true)
            ->andWhere('c.pic <> ?', '')
            ->orderBy('c.position ASC')
            ->execute();
    } // get clients slider

    public function executeGetAnnouncesSlider(sfWebRequest $request)
    {
        $this->res = [];

        $rows = Doctrine_Query::create()
            ->from('News n')
            ->where('n.is_active = ?', true)
            ->where('n.is_show_on_top = ?', true)
            ->orderBy('n.id DESC')
            ->limit(5)
            ->execute()
        ;

        foreach ($rows as $row) {
            $this->res[] = [
                'name' => $row->name,
                'announce' => $row->announce,
                'date' => $row->updated_at,
                'view_cnt' => $row->view_cnt,
                'url' => url_for('@news_view?slug='.$row->slug)
            ];
        }


        $rows = Doctrine_Query::create()
            ->from('Blog b')
            ->where('b.is_active = ?', true)
            ->where('b.announce_on_top = ?', true)
            ->orderBy('b.id DESC')
            ->limit(5)
            ->execute()
        ;

        foreach ($rows as $row) {
            $this->res[] = [
                'name' => $row->name,
                'announce' => $row->announce,
                'date' => $row->updated_at,
                'view_cnt' => $row->view_cnt,
                'url' => url_for('@blog_view?slug='.$row->slug)
            ];
        }

        shuffle($this->res);
    } // get announces slider

    public function executeGetEventsSlider(sfWebRequest $request)
    {
        $this->events_pics = Doctrine_Query::create()
            ->from('EventGallery eg')
            ->innerJoin('eg.Event e')
            ->where('eg.is_show_on_top = ?', true)
            ->andWhere('eg.pic <> ?', '')
            ->orderBy('eg.id DESC')
            ->execute();
    } // get events slider

    public function executeGetStaffList(sfWebRequest $request)
    {
        $this->staff_list = Doctrine_Core::getTable('Staff')->findAll();
    } // get staff list

    public function executeGetBanner(sfWebRequest $request)
    {
        $q = Doctrine_Query::create()
            ->from('Banner b')
            ->where('b.is_active = ?', true)
            ->orderBy('RAND()')
            ->limit(1);
        $this->banner = $q->fetchOne();

    } // get banner

    public function executeGetCategoriesTree(sfWebRequest $request)
    {
        $q = Doctrine_Query::create()
            ->select('c.*')
            ->from('Category c')
            ->addSelect('(SELECT count(*) FROM Product p WHERE p.category_id = c.id AND p.is_published = 1) as nb_products')
            ->where('c.company_id = ?', $request->getParameter('company_id', false))
            ->orderBy('c.root_id ASC, c.lft ASC');
        $this->categories = $q->execute();

    } // get categories

    public function executeGetRecalls()
    {
        $this->recalls = Doctrine_Query::create()
            ->from('Recall f')
            ->where('f.is_published = ?', true)
            ->andWhere('f.pic <> ?', '')
            ->orderBy('f.id DESC')
            ->limit(10)
            ->execute();
    } // get Recalls

    public function executeGetPopups()
    {
        $this->recall_form = new UserRecallForm();
    }
}