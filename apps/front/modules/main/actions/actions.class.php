<?php

/**
 * main actions.
 *
 * @package    scp
 * @subpackage main
 * @author     Your name here
 * @version    SVN: $Id$
 */
class mainActions extends sfActions
{
    /**
     * Executes index action
     *
     * @param sfRequest $request A request object
     */
    public function executeIndex(sfWebRequest $request)
    {
        $this->feedback_form = new FeedbackForm();
    }
}
