<?php use_helper('Thumbnail') ?>
<div class="section-header">
    <h2 class="section-title text-center wow fadeInDown">Сертификаты</h2>

    <p class="text-center wow fadeInDown">Наши сертификаты потдтверждают подлинность и безопасность</p>
</div>

<!--        <div class="text-center">-->
<!--            <ul class="portfolio-filter">-->
<!--                <li><a class="active" href="#" data-filter="*">All Works</a></li>-->
<!--                <li><a href="#" data-filter=".certificates">Certificates</a></li>-->
<!--                <li><a href="#" data-filter=".creative">Creative</a></li>-->
<!--                <li><a href="#" data-filter=".corporate">Corporate</a></li>-->
<!--                <li><a href="#" data-filter=".portfolio">Portfolio</a></li>-->
<!--            </ul><!--/#portfolio-filter-->
<!--        </div>-->

<div class="portfolio-items">
    <?php foreach ($items as $item): ?>
        <div class="portfolio-item certificates">

            <div class="portfolio-item-inner">
                <img class="img-responsive" src="<?php
                echo workshop_thumb(sfConfig::get('sf_web_dir') . '/uploads/certificate/' . $item->getPic(), 314, 210, 'certificate') ?>"
                     alt="">

                <div class="portfolio-info">
                    <h3><?php echo $item->name ?></h3>
                    <a class="preview" href="/uploads/certificate/<?php echo $item->pic ?>" rel="prettyPhoto"><i
                            class="fa fa-eye"></i></a>
                </div>
            </div>
        </div><!--/.certificate-item-->
    <?php endforeach; ?>
</div>


</div>

