<form id="main-contact-form" name="contact-form" method="post" action="<?php echo url_for('feedback/create') ?>">
    <?php echo $form->renderHiddenFields() ?>
    <div class="form-group">
        <input type="text" name="feedback[name]" class="form-control" placeholder="ФИО" required>
    </div>
    <div class="form-group">
        <input type="email" name="feedback[email]" class="form-control" placeholder="Email" required>
    </div>
    <div class="form-group">
        <input type="text" name="feedback[subject]" class="form-control" placeholder="Тема" required>
    </div>
    <div class="form-group">
        <textarea name="feedback[content]" class="form-control" rows="4" placeholder="Сообщение" required></textarea>
    </div>
    <button type="submit" class="btn btn-primary">Отправить</button>
</form>

<div id="myModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" style="width: 60%; padding-top: 200px;">
        <div class="modal-content">
            <div class="modal-body">
                <p>Ваше сообщение отправлено!</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div><!-- /.modal -->


<script>
//    $(document).ready(function () {
//        $('#main-contact-form').submit(function (event) {
//            $.ajax({
//                url: $(this).attr('action'),
//                method: "POST",
//                success: function (html) {
//                    $('.modal-body').html('<p>Ваше сообщение отправлено!</p>');
//                    $('#myModal').modal('show');
//                }
//            })
//
////            event.stopPropagation();
//            event.preventDefault();
//            return false;
//        })
//
//    });
</script>