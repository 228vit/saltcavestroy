<?php use_helper('Thumbnail') ?>
<div class="section-header">
    <h2 class="section-title text-center wow fadeInDown">Фотографии</h2>

    <p class="text-center wow fadeInDown">Фотографии наших работ говорят сами за себя</p>
</div>

<!--        <div class="text-center">-->
<!--            <ul class="foto-filter">-->
<!--                <li><a class="active" href="#" data-filter="*">All Works</a></li>-->
<!--                <li><a href="#" data-filter=".certificates">Certificates</a></li>-->
<!--                <li><a href="#" data-filter=".creative">Creative</a></li>-->
<!--                <li><a href="#" data-filter=".corporate">Corporate</a></li>-->
<!--                <li><a href="#" data-filter=".foto">foto</a></li>-->
<!--            </ul><!--/#foto-filter-->
<!--        </div>-->

<div class="foto-items">
    <?php foreach ($items as $item): ?>
        <div class="foto-item certificates">

            <div class="foto-item-inner">
                <img class="img-responsive" src="<?php
                echo workshop_thumb(sfConfig::get('sf_web_dir') . '/uploads/foto/' . $item->getPic(), 314, 210, 'certificate') ?>"
                     alt="">

                <div class="foto-info">
                    <h3><?php echo $item->name ?></h3>
                    <a class="preview" href="/uploads/foto/<?php echo $item->pic ?>" rel="prettyPhoto"><i
                            class="fa fa-eye"></i></a>
                </div>
            </div>
        </div><!--/.certificate-item-->
    <?php endforeach; ?>
</div>


</div>

