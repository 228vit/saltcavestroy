<div class="owl-carousel">
    <?php foreach ($sliders as $slider): ?>
    <div class="item" style="background-image: url(/uploads/slider/<?php echo $slider->pic ?>);">
        <div class="slider-inner">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="carousel-content">
                            <h2><?php echo $slider->getHeader(ESC_RAW) ?></h2>

                            <p><?php echo $slider->getCaption(ESC_RAW) ?></p>

<!--                            <a class="btn btn-primary btn-lg" href="--><?php //echo $slider->url ?><!--">Читать далее</a>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--/.item-->
    <?php endforeach; ?>
</div>
