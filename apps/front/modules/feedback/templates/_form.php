<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>

<form
    action="<?php echo url_for('feedback/' . ($form->getObject()->isNew() ? 'create' : 'update') . (!$form->getObject()->isNew() ? '?id=' . $form->getObject()->getId() : '')) ?>"
    method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
    <?php if (!$form->getObject()->isNew()): ?>
        <input type="hidden" name="sf_method" value="put"/>
    <?php endif; ?>
    <div class="note">
        Поля, помеченные <span class='markRequiredField'>*</span>, обязательны для заполнения
    </div>
    <table>
        <tfoot>
        <tr>
            <td></td>
            <td>
                <?php echo $form->renderHiddenFields(false) ?>
                <input type="submit" value="отправить"/>
            </td>
        </tr>
        </tfoot>
        <tbody>
        <?php echo $form->renderGlobalErrors() ?>
        <tr>
            <th><?php echo $form['name']->renderLabel() ?></th>
            <td>
                <?php echo $form['name']->renderError() ?>
                <?php echo $form['name'] ?>
            </td>
        </tr>
        <tr>
            <th><?php echo $form['phone']->renderLabel() ?></th>
            <td>
                <?php echo $form['phone']->renderError() ?>
                <?php echo $form['phone'] ?>
            </td>
        </tr>
        <tr>
            <th><?php echo $form['email']->renderLabel() ?></th>
            <td>
                <?php echo $form['email']->renderError() ?>
                <?php echo $form['email'] ?>
            </td>
        </tr>

        <tr>
            <th><?php echo $form['subject']->renderLabel() ?></th>
            <td>
                <?php echo $form['subject']->renderError() ?>
                <?php echo $form['subject'] ?>
            </td>
        </tr>
        <tr>
            <th><?php echo $form['content']->renderLabel() ?></th>
            <td>
                <?php echo $form['content']->renderError() ?>
                <?php echo $form['content'] ?>
            </td>
        </tr>

        <tr id="antispam">
            <th><?php echo $form['captcha']->renderLabel() ?></th>
            <td>
                <?php echo $form['captcha']->renderError() ?>
                <?php echo $form['captcha'] ?>
            </td>
        </tr>

        </tbody>
    </table>
</form>


<script>
    $('#antispam').hide();
</script>