<?php

/**
 * feedback actions.
 *
 * @package    itworks
 * @subpackage feedback
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class feedbackActions extends sfActions
{

    public function executeNew(sfWebRequest $request)
    {
        $this->form = new FeedbackForm();
    }

    public function executeCreate(sfWebRequest $request)
    {
        $this->forward404Unless($request->isMethod(sfRequest::POST));

        $this->form = new FeedbackForm();

        $this->processForm($request, $this->form);

        $this->setTemplate('new');
    }

    public function executeSuccess(sfWebRequest $request)
    {
    }

    public function executeFail(sfWebRequest $request)
    {
    }

    protected function processForm(sfWebRequest $request, sfForm $form)
    {
        $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
        if ($form->isValid()) {
            /* @var $feedback Feedback */
            $feedback = $form->save();

            $message = Swift_Message::newInstance()
                ->setFrom(myConfig::get('noreply.email', 'saltcavestroy@yandex.ru'))
                ->setReplyTo($feedback->email)
                ->setTo(myConfig::get('admin.email', '228vit@gmail.com'))
                ->setSubject('new feedback: ' . $feedback->subject)
                ->setContentType('text/plain')
                ->setBody($feedback->content)
            ;

            $this->getMailer()->sendNextImmediately()->send($message, $failedRecipients);

            $this->redirect('feedback/success');
        } else {
            $this->redirect('feedback/fail');
        }

        $this->getUser()->setFlash('error', 'form is not valid');
    }
}
