<?php

/**
 * page actions.
 *
 * @package    accent
 * @subpackage page
 * @author     Your name here
 * @version    SVN: $Id$
 */
class pageActions extends sfActions
{
    /**
     * Executes index action
     *
     * @param sfRequest $request A request object
     */

    public function executeIndex(sfWebRequest $request)
    {
        $this->redirect('@homepage');
    }

    public function executeView(sfWebRequest $request)
    {
        $this->page = Doctrine_Core::getTable('Page')->findOneBySlug(
            $request->getParameter('slug', 'about')
        );
        $this->forward404Unless($this->page);

        $this->getResponse()->setTitle($this->page->title . ' / ' . myConfig::get('company_title', 'Студия Акцент'));

    }

    public function executeAbout(sfWebRequest $request)
    {
        $this->page = Doctrine_Core::getTable('Page')->findOneBySlug('about');
        $this->forward404Unless($this->page);

        $this->getResponse()->setTitle($this->page->title . ' / ' . myConfig::get('company_title', 'Студия Акцент'));

    }
}
