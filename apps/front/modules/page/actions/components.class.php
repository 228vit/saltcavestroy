<?php

class pageComponents extends sfComponents
{
    public function executeGetPageBySlug(sfWebRequest $request)
    {
        $this->getPage($this->slug);
    }

    public function executeGetRawPageBySlug(sfWebRequest $request)
    {
        $this->getPage($this->slug);
    }

    public function executeGetSimplePageBySlug(sfWebRequest $request)
    {
        $this->getPage($this->slug);
    }

    public function executeGetWowPageBySlug(sfWebRequest $request)
    {
        $this->getPage($this->slug);
    }

    private function getPage($slug)
    {
        $this->page = Doctrine_Query::create()
            ->from('Page p')
            ->where('p.slug = ?', $slug)
            ->fetchOne()
        ;
    }

    public function executeGetProductCategories(sfWebRequest $request)
    {

        $this->product_categories = Doctrine_Query::create()
            ->from('ProductCategory pc')
            ->orderBy('pc.position ASC')
            ->execute();

    }

}