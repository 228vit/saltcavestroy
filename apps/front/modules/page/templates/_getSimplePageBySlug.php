<?php if ($page): ?>
    <div class="section-header">
        <h2><?php echo $page->getName(ESC_RAW) ?></h2>
    </div>
    <p>
        <?php echo $page->getContent(ESC_RAW) ?>
    </p>
<?php else: ?>
    <p>---</p>
<?php endif; ?>
