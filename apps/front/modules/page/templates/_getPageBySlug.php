<?php if ($page): ?>
    <div class="section-header">
        <h2 class="section-title text-center wow fadeInDown animated"><?php echo $page->getName(ESC_RAW) ?></h2>
    </div>
    <div class="row">
        <p>
            <?php echo $page->getContent(ESC_RAW) ?>
        </p>
    </div>
<?php else: ?>
    <p>---</p>
<?php endif; ?>
