<?php if ($page): ?>
    <h2 class="section-title text-center wow fadeInDown"><?php echo $page->getName(ESC_RAW) ?></h2>
    <p class="text-center wow fadeInDown">
        <?php echo $page->getContent(ESC_RAW) ?>
    </p>
<?php else: ?>
    <p>---</p>
<?php endif; ?>
