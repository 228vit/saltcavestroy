<section id="about_orig">
    <div class="container">
        <div class="section-header">
            <h2 class="section-title text-center wow fadeInDown">О нас</h2>

            <p class="text-center wow fadeInDown">Наша деятельность не ограничивается строительством соляных пещер, мы:</p>
        </div>
        <div class="row">
            <div class="col-sm-6 wow fadeInLeft">
                <img class="img-responsive" src="/images/main-feature.png" alt="">
            </div>
            <div class="col-sm-6">
                <div class="media service-box wow fadeInRight">
                    <div class="pull-left">
                        <i class="fa fa-line-chart"></i>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">производим</h4>

                        <p>производим оборудование, занимаемся научным обоснованием применения галотерапии на практике в различных областях медицины</p>
                    </div>
                </div>

                <div class="media service-box wow fadeInRight">
                    <div class="pull-left">
                        <i class="fa fa-cubes"></i>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">разрабатываем методические рекомендации</h4>

                        <p>Совместно с ведущими медицинскими учреждениями страны разрабатываем методические рекомендации по применению этого инновационного метода</p>
                    </div>
                </div>

                <div class="media service-box wow fadeInRight">
                    <div class="pull-left">
                        <i class="fa fa-pie-chart"></i>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">поставляем ГалоСПА</h4>

                        <p>Помимо медицинского оборудования, у нас есть линейка бытовых галогенераторов, получивших широкое применение в СПА индустрии-технология ГалоСПА</p>
                    </div>
                </div>

                <div class="media service-box wow fadeInRight">
                    <div class="pull-left">
                        <i class="fa fa-pie-chart"></i>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">SEO services</h4>

                        <p>Backed by some of the biggest names in the industry, Firefox OS is an open platform that
                            fosters greater</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>