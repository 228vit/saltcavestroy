<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>SaltcaveStroy.Ru</title>
    <!-- core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet">
    <link href="/css/animate.min.css" rel="stylesheet">
    <link href="/css/owl.carousel.css" rel="stylesheet">
    <link href="/css/owl.transitions.css" rel="stylesheet">
    <link href="/css/prettyPhoto.css" rel="stylesheet">
    <link href="/css/main.css" rel="stylesheet">
    <link href="/css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="/js/html5shiv.js"></script>
    <script src="/js/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="/images/ico/apple-touch-icon-57-precomposed.png">
    <script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=ufYbExTFEX1DwOp8S4MD7xMWlbIGqBQ63ktvxA0ZSgYdzI*qN4s0W/cWLWrrb4*3TbaioPLeiR76PzGPU8mlTVigtvdDJrFhplt2MQkKy3iVrqTYcyjD88mW8uI7AOsbzJy2UORAR8646s4Cc3ID6OnKWgMtdkLoQ4mTmrqCirQ-&pixel_id=1000039922';</script>
</head><!--/head-->

<body id="home" class="homepage">

<header id="header">
    <nav id="main-menu" class="navbar navbar-default navbar-fixed-top" role="banner">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo url_for('@homepage') ?>" style="padding-top: 2px;"><img src="images/logo5.png" alt="logo" style="width: 280px;"></a>
            </div>

            <div class="collapse navbar-collapse navbar-right">
                <ul class="nav navbar-nav">
                    <li><a href="<?php echo url_for('homepage') ?>">Главная</a></li>
                    <li><a href="<?php echo url_for('homepage') ?>#about">О нас</a></li>
                    <li class="scroll"><a href="<?php echo url_for('homepage') ?>#services">Мы предлагаем</a></li>
                    <li class="scroll"><a href="<?php echo url_for('homepage') ?>#franshiza">Франшиза</a></li>
                    <li class="scroll"><a href="<?php echo url_for('homepage') ?>#projects">Проекты</a></li>
                    <li class="scroll"><a href="<?php echo url_for('homepage') ?>#portfolio">Сертификаты</a></li>
                    <li class="scroll"><a href="<?php echo url_for('homepage') ?>#foto">Фото</a></li>
                    <li class="scroll"><a href="<?php echo url_for('homepage') ?>#get-in-touch">Контакты</a></li>
                    <li class="active"><a href="<?php echo url_for('articles') ?>">Полезное</a></li>
                </ul>
            </div>
        </div><!--/.container-->
    </nav><!--/nav-->
</header><!--/header-->

<?php $phone = myConfig::get('main_phone', '+7 (812) 6451875') ?>

<section id="get-in-touch">
    <div class="container">
        <div class="section-header">
            <h2 class="section-title text-center wow fadeInDown animated" style="visibility: visible; animation-name: fadeInDown;">Статьи и полезная информация</h2>
            <p class="text-center wow fadeInDown animated" style="visibility: visible; animation-name: fadeInDown;">
                материалы на тему галотерапии
            </p>
        </div>
    </div>
</section>
<section id="articles">
    <div class="container">
        <?php echo $sf_content ?>
    </div>
</section><!--/#about-->


<section id="get-in-touch">
    <div class="container">
        <div class="section-header">
            <h2 class="section-title text-center wow fadeInDown">Мы на связи</h2>
            <p class="text-center wow fadeInDown">
                По любому вопросу Вы можете нам позвонить, написать, или посетить наш офис.
            </p>
        </div>
    </div>
</section><!--/#get-in-touch-->


<section id="contact">
    <div id="google-map" style="height:650px" data-latitude="60.009051" data-longitude="30.2439062"></div>
    <div class="container-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-sm-offset-8">
                    <div class="contact-form">
                        <h3>Контакты</h3>

                        <address>
                            <strong><?php echo  myConfig::get('company_name', 'SaltCave Stroy') ?></strong><br>
                            <?php echo myConfig::get('address','197373, Санкт-Петербург, пр.Авиаконструкторов дом 2') ?><br>
                            <abbr title="Phone">тел:</abbr> <a href="tel:<?php echo $phone ?>"><?php echo $phone ?></a>
                            <br>

                            <?php $p = myConfig::get('phone_manager1','+7 903 0921372') ?>
                            <?php if ($p): ?>
                                Ваш прямой менеджер: <a href="tel:<?php echo $p ?>"><?php echo $p ?></a>
                                <br>
                            <?php endif; ?>

                            <?php $p = myConfig::get('phone_manager2','+7 905 2330153') ?>
                            <?php if ($p): ?>
                                Ваш прямой менеджер: <a href="tel:<?php echo $p ?>"><?php echo $p ?></a>
                                <br>
                            <?php endif; ?>

                            <?php $p = myConfig::get('phone_manager3','') ?>
                            <?php if ($p): ?>
                                Ваш прямой менеджер: <a href="tel:<?php echo $p ?>"><?php echo $p ?></a>
                                <br>
                            <?php endif; ?>

                            Сервис:
                            тел./факс: <a href="tel:<?php echo $p = myConfig::get('phone_service','+7(812) 363-47-41') ?>"><?php echo $p ?></a>
                            <br>

                            <?php $p = myConfig::get('skype','saltcavestroy') ?>
                            <?php if ($p): ?>
                                Skype: <a href="skype:<?php echo $p ?>"><?php echo $p ?></a>
                                <br>
                            <?php endif; ?>
                            <?php $p = myConfig::get('email','info@saltcave.ru') ?>
                            <?php if ($p): ?>
                                Email: <a href="mailto:<?php echo $p ?>"><?php echo $p ?></a>
                                <br>
                            <?php endif; ?>

                            <?php $p = myConfig::get('vk', 'https://vk.com/saltcavestroy') ?>
                            <?php if ($p): ?>
                                Группа ВКонтакте: <a target="_blank" href="<?php echo $p ?>"><?php echo $p ?></a>
                                <br>
                            <?php endif; ?>

                        </address>

                        <?php include_component('main', 'feedbackForm') ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!--/#bottom-->

<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                &copy; <?php echo date("Y") ?> SaltCave Stroy. Designed by <a target="_blank" href="http://shapebootstrap.net/" title="Free Twitter Bootstrap WordPress Themes and HTML templates">ShapeBootstrap</a>
            </div>
            <div class="col-sm-4">
                <ul class="social-icons">
                    <li><a target="_blank" href="<?php echo $p ? $p : '#'?>"><i class="fa fa-vk"></i></a></li>
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                </ul>
            </div>
            <div class="col-sm-2">
                <?php include_partial('global/yandex_metrika') ?>
            </div>
        </div>
    </div>
</footer><!--/#footer-->

<script src="/js/jquery.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
<script src="/js/owl.carousel.min.js"></script>
<script src="/js/mousescroll.js"></script>
<script src="/js/smoothscroll.js"></script>
<script src="/js/jquery.prettyPhoto.js"></script>
<script src="/js/jquery.isotope.min.js"></script>
<script src="/js/jquery.inview.min.js"></script>
<script src="/js/wow.min.js"></script>
<script src="/js/main.js"></script>
</body>
</html>
