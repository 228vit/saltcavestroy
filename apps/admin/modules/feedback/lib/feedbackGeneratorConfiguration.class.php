<?php

/**
 * feedback module configuration.
 *
 * @package    scp
 * @subpackage feedback
 * @author     Your name here
 * @version    SVN: $Id: configuration.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class feedbackGeneratorConfiguration extends BaseFeedbackGeneratorConfiguration
{
  public function getPagerMaxPerPage()
  {
    $user = sfContext::getInstance()->getUser();
    return $user->getAttribute('max_per_page', 50);
  }

  public function getFormActions()
  {
    return array(  '_delete' => NULL,  '_list' => NULL,  '_save' => NULL,  '_save_and_add' => NULL,  '_save_and_list' => NULL,);
  }

  public function getNewActions()
  {
    return array();
  }

  public function getEditActions()
  {
    return array(  '_delete' => NULL,  '_list' => NULL,  '_save' => NULL,  '_save_and_add' => NULL,  '_save_and_list' => NULL,);
  }

}
