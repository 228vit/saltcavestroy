<?php use_helper('Thumbnail') ?>
<?php if ($foto->getPic()): ?>
    <?php echo link_to(
        '<img src="'.workshop_thumb(
            ($foto->getPic() ? sfConfig::get('sf_web_dir') . '/uploads/foto/'.$foto->getPic() : sfConfig::get('sf_web_dir') . '/uploads/no-pic.jpg'),
            150, 50, 'slider').'" border="1" />',
        'slider_edit', $foto) ?>
<?php else: ?>
    <img src="http://placehold.it/50x50" />
<?php endif;?>