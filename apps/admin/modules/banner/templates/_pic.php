<?php use_helper('Thumbnail') ?>

<?php if ($banner->getPic()): ?>
    <?php echo link_to(
        '<img src="'.workshop_thumb(
            ($banner->getPic() ? sfConfig::get('sf_web_dir') . '/uploads/promo/'.$banner->getPic() : sfConfig::get('sf_web_dir') . '/uploads/no-pic.jpg'),
            50, 50, 'slider').'" border="1" />',
        'slider_edit', $banner) ?>
<?php else: ?>
    <img src="http://placehold.it/50x50" />
<?php endif;?>