<?php use_helper('I18N') ?>

<?php include_partial('aceAdminDash/flashes') ?>

    <h2 class="text-danger"><i class="icon-exclamation-sign"></i>
        The page you asked for is secure and you do not have proper credentials.</h2>

<p><?php echo sfContext::getInstance()->getRequest()->getUri() ?></p>

