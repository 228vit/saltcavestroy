<?php use_helper('Thumbnail') ?>
<?php if ($slider->getPic()): ?>
    <?php echo link_to(
        '<img src="'.workshop_thumb(
            ($slider->getPic() ? sfConfig::get('sf_web_dir') . '/uploads/slider/'.$slider->getPic() : sfConfig::get('sf_web_dir') . '/uploads/no-pic.jpg'),
            150, 50, 'slider').'" border="1" />',
        'slider_edit', $slider) ?>
<?php else: ?>
    <img src="http://placehold.it/50x50" />
<?php endif;?>