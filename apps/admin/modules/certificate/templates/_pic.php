<?php use_helper('Thumbnail') ?>

<?php if ($certificate->getPic()): ?>
    <?php echo link_to(
        '<img src="'.workshop_thumb(
            ($certificate->getPic() ? sfConfig::get('sf_web_dir') . '/uploads/certificate/'.$certificate->getPic() : sfConfig::get('sf_web_dir') . '/uploads/no-pic.jpg'),
            50, 50, 'certificate').'" border="1" />',
        'certificate_edit', $certificate) ?>
<?php else: ?>
    <img src="http://placehold.it/50x50" />
<?php endif;?>