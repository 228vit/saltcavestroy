<?php

/**
 * page module helper.
 *
 * @package    scp
 * @subpackage page
 * @author     Your name here
 * @version    SVN: $Id: helper.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class pageGeneratorHelper extends BasePageGeneratorHelper
{

    public function linkToNew($params)
    {
        return '<li>' . link_to('Create', '@' . $this->getUrlForAction('new')) . '</li>';
    }

    public function linkToEdit($object, $params)
    {
        return link_to(
            '<i class="icon-pencil bigger-130"></i>',
            $this->getUrlForAction('edit'),
            $object,
            array('class' => 'green')) .'</li>';

        // return '<li class="sf_admin_action_edit">' . link_to(__($params['label'], array(), 'sf_admin'), $this->getUrlForAction('edit'), $object) . '</li>';
    }

    public function linkToDelete($object, $params)
    {
        if ($object->isNew()) {
            return '';
        }

        return link_to(
            '<i class="icon-trash bigger-130"></i>',
            $this->getUrlForAction('delete'),
            $object,
            array('class' => 'red',
                'method' => 'delete',
                'confirm' => !empty($params['confirm']) ? __($params['confirm'], array(), 'sf_admin') : $params['confirm']
            )
        );
        // return '<li class="sf_admin_action_delete">' . link_to(__($params['label'], array(), 'sf_admin'), $this->getUrlForAction('delete'), $object, array('method' => 'delete', 'confirm' => !empty($params['confirm']) ? __($params['confirm'], array(), 'sf_admin') : $params['confirm'])) . '</li>';
    }

    public function linkToList($params)
    {
        return '<li class="sf_admin_action_list">' . link_to(__($params['label'], array(), 'sf_admin'), '@' . $this->getUrlForAction('list')) . '</li>';
    }

    public function linkToSave($object, $params)
    {
        return '<li class="sf_admin_action_save"><input type="submit" value="' . __($params['label'], array(), 'sf_admin') . '" /></li>';
    }

    public function linkToSaveAndAdd($object, $params)
    {
        if (!$object->isNew()) {
            return '';
        }

        return '<li class="sf_admin_action_save_and_add"><input type="submit" value="' . __($params['label'], array(), 'sf_admin') . '" name="_save_and_add" /></li>';
    }
}
