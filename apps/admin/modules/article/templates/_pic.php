<?php use_helper('Thumbnail') ?>

<?php if ($article->getPic()): ?>
    <?php echo link_to(
        '<img src="'.workshop_thumb(
            ($article->getPic() ? sfConfig::get('sf_web_dir') . '/uploads/promo/'.$article->getPic() : sfConfig::get('sf_web_dir') . '/uploads/no-pic.jpg'),
            50, 50, 'slider').'" border="1" />',
        'slider_edit', $article) ?>
<?php else: ?>
    <img src="http://placehold.it/50x50" />
<?php endif;?>