<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <?php include_http_metas() ?>
  <?php include_metas() ?>
  <?php include_title() ?>
  <link rel="shortcut icon" href="/favicon.png"/>
  <?php include_stylesheets() ?>
  <?php include_javascripts() ?>
</head>
<body>

<?php //include_component('sfAdminDash', 'header'); ?>

<?php echo $sf_content ?>

<?php include_partial('sfAdminDash/footer'); ?>
<script>
  $(function () {

    $(".various .fancybox").fancybox({
      maxWidth: 1280,
      maxHeight: 1024,
      fitToView: false,
      width: '90%',
      height: '90%',
      autoSize: false,
      closeClick: false,
      openEffect: 'none',
      closeEffect: 'none'
    });

    $('.confirmMe').click(function () {
      return confirm("Вы уверены?");
    });

    function showProcess() {
      var popupX = Math.round(($(window).width() - $("#result_div").width()) / 2);
      var popupY = $(document).scrollTop() + Math.round($(window).height() / 2) - Math.round($("#result_div").height() / 2);
      $("#result_div").css({top: popupY + "px", left: popupX + "px"});
      var pic_dir = $(this).attr("pic_dir")
      var pic_id = $(this).attr("rel")
      $("#result_div").slideDown("slow");
    }

    function hideProcess() {
      $("#result_div").slideUp("slow");
    }

    $(".switch_status").each(function () {
      $(this).click(function () {
        var popupX = Math.round(($(window).width() - $("#result_div").width()) / 2);
        var popupY = $(document).scrollTop() + Math.round($(window).height() / 2) - Math.round($("#result_div").height() / 2);
        $("#result_div").css({top: popupY + "px", left: popupX + "px"});
        var pic_dir = $(this).attr("pic_dir")
        var pic_id = $(this).attr("rel")
        showProcess();

        $.ajax({
          type: "POST",
          url: $(this).attr('href'),
          data: "",
          success: function (msg) {
            var pic_name = (msg == 1 ? "ok" : "cancel") + ".png"
            $("#" + pic_id).attr("src", pic_dir + "/" + pic_name)
            hideProcess();
          }
        });//ajax
        return false;
      })//click
    })//each
  });
</script>
</body>
</html>
