<?php

class adminConfiguration extends sfApplicationConfiguration
{
    public function configure()
    {
        sfConfig::set('sf_app_template_dir', sfConfig::get('sf_plugins_dir') . '/aceAdminPlugin/templates');
    }

    public function getDecoratorDirs()
    {
        return sfConfig::get('app_template_dirs', parent::getDecoratorDirs());
    }
}
