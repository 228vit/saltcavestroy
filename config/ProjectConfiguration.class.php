<?php

require_once __DIR__ . '/../lib/vendor/symfony/lib/autoload/sfCoreAutoload.class.php';
sfCoreAutoload::register();

class ProjectConfiguration extends sfProjectConfiguration
{
    public function setup()
    {

        $this->enablePlugins('sfDoctrinePlugin');
        $this->enablePlugins('sfDoctrineGuardPlugin');
        $this->enablePlugins('myConfigPlugin');
        $this->enablePlugins('aceAdminPlugin');
        $this->enablePlugins('sfFormExtraPlugin');
        $this->enablePlugins('sfCKEditorPlugin');
        $this->enablePlugins('sfImageTransformPlugin');
        $this->enablePlugins('sfThumbnailPlugin');
        $this->enablePlugins('csDoctrineActAsSortablePlugin');
    }
}
