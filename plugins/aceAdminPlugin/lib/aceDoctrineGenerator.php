<?php
class aceDoctrineGenerator extends sfDoctrineGenerator
{
    /**
     * Returns HTML code for an action link.
     *
     * @param string  $actionName The action name
     * @param array   $params     The parameters
     * @param boolean $pk_link    Whether to add a primary key link or not
     *
     * @return string HTML code
     */
    public function getLinkToAction($actionName, $params, $pk_link = false)
    {
        $action = isset($params['action']) ? $params['action'] : 'List'.sfInflector::camelize($actionName);

        $url_params = $pk_link ? '?'.$this->getPrimaryKeyUrlParams() : '\'';

        return '[?php echo link_to(\''.$params['label'].'\'), \''.$this->getModuleName().'/'.$action.$url_params.', '.$this->asPhp($params['params']).') ?]';
    }

    public function getBtnToAction($actionName, $params, $pk_link = false)
    {
        $action = isset($params['action']) ? $params['action'] : 'List'.sfInflector::camelize($actionName);

        $url_params = $pk_link ? '?'.$this->getPrimaryKeyUrlParams() : '\'';

        $icon = isSet($params['icon']) ? $params['icon'] : 'info';
        $class = isSet($params['class']) ? $params['class'] : 'success';
        $label = isSet($params['label']) ? $params['label'] : 'tooltip';
        $popup = isSet($params['popup']) ? $params['popup'] : false;
        $ajax = isSet($params['ajax']) ? $params['ajax'] : false;
        $ajax_confirm = isSet($params['ajax_confirm']) ? $params['ajax_confirm'] : false;

        if ($ajax) { $class .= " ajax"; }
        if ($ajax_confirm) { $class .= " ajax_confirm"; }
        $link_params = [
            'data-rel' => 'tooltip',
            'title' => $label,
            'data-original-title' => 'Default tooltip',
            'class' => "btn btn-{$class}",
            'confirm' => !empty($params['confirm']) ? $params['confirm'] : false
        ];

        if ($popup) {
            $link_params['data-toggle'] = 'modal';
            $link_params['data-target'] = '#popupModal';
        }

        $link_text = '<i class="icon-'.$icon.' bigger-130" data-rel="tooltip" data-original-title="'.$label.'"></i> '.$label;
        $path = $this->getModuleName().'/'.$action.$url_params;

        return '[?php echo link_to(\''.$link_text.'\', \''.$path.', '.$this->asPhp($link_params).') ?]';
    }
}