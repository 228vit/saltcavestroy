<?php

abstract class aceAdminModelGeneratorHelper extends sfModelGeneratorHelper
{

    public function linkToSaveAndList($object, $params)
    {
        return '<li class="sf_admin_action_save_and_list"><input type="submit" value="' . __($params['label'], array(), 'sf_admin') . '" name="_save_and_list" /></li>';
    }

    public function linkToSaveAndNext($object, $params)
    {
        if ($this->module->getSecurityManager()->userHasCredentials('edit', $object))
            return '<li class="sf_admin_action_save_and_next"><input class="green" type="submit" value="' . __($params['label'], array(), 'dm') . '" name="_save_and_next" /></li>';
    }


}