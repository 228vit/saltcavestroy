<?php

/*
 * This file is part of the symfony package.
 * (c) Fabien Potencier <fabien.potencier@symfony-project.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * sfValidatorDateRange validates a range of date. It also converts the input values to valid dates.
 *
 * @package    symfony
 * @subpackage validator
 * @author     Fabien Potencier <fabien.potencier@symfony-project.com>
 * @version    SVN: $Id: sfValidatorDateRange.class.php 32810 2011-07-21 05:18:56Z fabien $
 */
class aceValidatorDateRange extends sfValidatorBase
{
    /**
     * Configures the current validator.
     *
     * Available options:
     *
     *  * from_date:   The from date validator (required)
     *  * to_date:     The to date validator (required)
     *  * from_field:  The name of the "from" date field (optional, default: from)
     *  * to_field:    The name of the "to" date field (optional, default: to)
     *
     * @param array $options    An array of options
     * @param array $messages   An array of error messages
     *
     * @see sfValidatorBase
     */
    protected function configure($options = array(), $messages = array())
    {
        $this->setMessage('invalid', 'The begin date must be before the end date.');

        $this->addOption('required', FALSE);
        $this->addOption('dates_separator', ' / ');

//        $this->addRequiredOption('from_date');
//        $this->addRequiredOption('to_date');
//        $this->addOption('from_field', 'from');
//        $this->addOption('to_field', 'to');
    }

    /**
     * @see sfValidatorBase
     */
    protected function doClean($value)
    {
        // slice 2 dates
        $separator = $this->getOption('dates_separator');
        $parts = explode($separator, $value);

        $date_from = isSet($parts[0]) ? $parts[0] : NULL;
        $date_to = isSet($parts[1]) ? $parts[1] : NULL;

        // convert to sf_format, and check if date types supported
        $sf_date_from = strtotime($date_from) ? date('Y-m-d', strtotime($date_from)) : NULL;
        $sf_date_to = strtotime($date_to) ? date('Y-m-d', strtotime($date_to)) : NULL;

        // convert to array
        $v_from_date = new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00'));
        $v_to_date = new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59'));

        $value = [
            'from' => $v_from_date->clean($sf_date_from ? $sf_date_from : null),
            'to' => $v_to_date->clean($sf_date_to ? $sf_date_to : null),
        ];

        if ($value['from'] && $value['to']) {

            $v = new sfValidatorSchemaCompare(
                $value['from'],
                    sfValidatorSchemaCompare::LESS_THAN_EQUAL,
                $value['to'],
                array('throw_global_error' => true),
                array('invalid' => $this->getMessage('invalid'))
            );
            $v->clean($value);
        }

        return $value;
    }
}
