<?php

/*
 * This file is part of the symfony package.
 * (c) Fabien Potencier <fabien.potencier@symfony-project.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * aceWidgetFormDaterange represents daterange widget.
 *
 * This widget needs JQuery to work.
 *
 * You also need to include the JavaScripts and stylesheets files returned by the getJavaScripts()
 * and getStylesheets() methods.
 *
 * @package    aceAdmin
 * @subpackage widget
 * @author     vit <228vit@gmail.com>
 * @version    1.0
 */
class aceWidgetFormDateOld extends sfWidgetFormInput
{
    /**
     * Configures the current widget.
     *
     * Available options:
     *
     *  * url:            The URL to call to get the choices to use (required)
     *  * config:         A JavaScript array that configures the JQuery autocompleter widget
     *  * value_callback: A callback that converts the value before it is displayed
     *
     * @param array $options     An array of options
     * @param array $attributes  An array of default HTML attributes
     *
     * @see sfWidgetForm
     */
    protected function configure($options = array(), $attributes = array())
    {
        $this->addOption('with_empty', false);
        $this->addOption('empty_label', 'is empty');
        $this->addOption('placeholder', 'date');
        $this->addOption('js_date_format', 'dd.mm.yyyy');
        $this->addOption('php_date_format', 'd.m.Y');

        $this->addOption('date_start', date('Y-m-d'));
        $this->addOption('date_start', false);
        $this->addOption('config', '{ }');
        $this->addOption('input_attributes', []);
        $this->addOption('input_hidden_attributes', []);

    }

    /**
     * @param  string $name        The element name
     * @param  string $value       The date displayed in this widget
     * @param  array $attributes  An array of HTML attributes to be merged with the default HTML attributes
     * @param  array $errors      An array of errors for the field
     *
     * @return string An HTML tag string
     *
     * @see sfWidgetForm
     */
    public function render($name, $value = null, $attributes = array(), $errors = array())
    {
        $php_date_format = $this->getOption('php_date_format');
        $js_date_format = $this->getOption('js_date_format');
        $html_id = $this->generateId($name);
        $attributes['class'] = 'form-control date-picker';
        $attributes['data-date-format'] = $js_date_format;

        if (!empty($value)) {
            $value = date($php_date_format, strtotime($value));
        } else {
            $value = '';
        }


        return '<div class="input-group col-sm-4 no-padding">'
            .
            $this->renderTag('input', array_merge(
                array(
                    'type' => 'text', 'id' => $html_id,
                    'name' => $name, 'value' => $value),
                $attributes
            ))
            .
            '<span class="input-group-addon">
                <i class="icon-calendar bigger-110"></i>
            </span>
            </div>'
            .
            "
            <script type='text/javascript'>
                $(function() {
                    $('#{$html_id}').datepicker({
                            autoclose:true
                    }).next().on(ace.click_event, function(){
                        $(this).prev().focus();
                    });
                });
            </script>
            "
        ;
    }

    /**
     * Gets the stylesheet paths associated with the widget.
     *
     * @return array An array of stylesheet paths
     */
    public function getStylesheets()
    {
        return array('/aceAdminPlugin/css/datepicker.css' => 'all');
    }

    /**
     * Gets the JavaScript paths associated with the widget.
     *
     * @return array An array of JavaScript paths
     */
    public function getJavascripts()
    {
        return array(
            '/aceAdminPlugin/js/date-time/bootstrap-datepicker.min.js',
            '/aceAdminPlugin/js/date-time/moment.min.js'
        );
    }
}
