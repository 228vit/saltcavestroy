<?php

/*
 * This file is part of the symfony package.
 * (c) Fabien Potencier <fabien.potencier@symfony-project.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * sfWidgetFormInput represents an HTML text input tag.
 *
 * @package    symfony
 * @subpackage widget
 * @author     Fabien Potencier <fabien.potencier@symfony-project.com>
 * @version    SVN: $Id: sfWidgetFormInputText.class.php 30762 2010-08-25 12:33:33Z fabien $
 */
class aceWidgetFormInputSpinner extends sfWidgetFormInput
{
    /**
     * Configures the current widget.
     *
     * @param array $options     An array of options
     * @param array $attributes  An array of default HTML attributes
     *
     * @see sfWidgetForm
     */
    protected function configure($options = array(), $attributes = array())
    {
        parent::configure($options, $attributes);

        $this->setOption('type', 'text');
    }

    /**
     * @param  string $name        The element name
     * @param  string $value       The date displayed in this widget
     * @param  array $attributes  An array of HTML attributes to be merged with the default HTML attributes
     * @param  array $errors      An array of errors for the field
     *
     * @return string An HTML tag string
     *
     * @see sfWidgetForm
     */
    public function render($name, $value = null, $attributes = array(), $errors = array())
    {
        // '' transform to 0
        $value = intval($value);
        $html_id = $this->generateId($name);
        // alert! important attribute to make field taggable
        $attributes['class'] = 'input-mini ace_spinner';

        return $this->renderTag('input', array_merge(
            array(
                'type' => 'text', 'id' => $html_id,
                'name' => $name, 'value' => $value),
            $attributes
        )).
"<script>
    jQuery(function($) {
        $('.ace_spinner').ace_spinner({value:{$value}, min:0, max:100, step:1, btn_up_class:'btn-info', btn_down_class:'btn-info'})
    })
</script>";

    }

    /**
     * Gets the JavaScript paths associated with the widget.
     *
     * @return array An array of JavaScript paths
     */
    public function getJavascripts()
    {
        return array(
            '/aceAdminPlugin/js/fuelux/fuelux.spinner.min.js',
        );
    }
}
