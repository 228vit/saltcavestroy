<?php

/*
 * This file is part of the symfony package.
 * (c) Fabien Potencier <fabien.potencier@symfony-project.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * aceWidgetFormDaterange represents daterange widget.
 *
 * This widget needs JQuery to work.
 *
 * You also need to include the JavaScripts and stylesheets files returned by the getJavaScripts()
 * and getStylesheets() methods.
 *
 * @package    aceAdmin
 * @subpackage widget
 * @author     vit <228vit@gmail.com>
 * @version    1.0
 */
class aceWidgetFormDate extends sfWidgetFormInput
{
    /**
     * Configures the current widget.
     *
     * Available options:
     *
     *  * url:            The URL to call to get the choices to use (required)
     *  * config:         A JavaScript array that configures the JQuery autocompleter widget
     *  * value_callback: A callback that converts the value before it is displayed
     *
     * @param array $options     An array of options
     * @param array $attributes  An array of default HTML attributes
     *
     * @see sfWidgetForm
     */
    protected function configure($options = array(), $attributes = array())
    {
        $this->addOption('with_empty', false);
        $this->addOption('empty_label', 'is empty');
        $this->addOption('placeholder', 'date');
        $this->addOption('js_date_format', 'dd.MM.yyyy');
        $this->addOption('php_date_format', 'd.m.Y');

        $this->addOption('date_start', date('Y-m-d'));
        $this->addOption('date_start', false);
        $this->addOption('config', '{ }');
        $this->addOption('input_attributes', []);
        $this->addOption('input_hidden_attributes', []);

    }

    /**
     * @param  string $name        The element name
     * @param  string $value       The date displayed in this widget
     * @param  array $attributes  An array of HTML attributes to be merged with the default HTML attributes
     * @param  array $errors      An array of errors for the field
     *
     * @return string An HTML tag string
     *
     * @see sfWidgetForm
     */
    public function render($name, $value = null, $attributes = array(), $errors = array())
    {
        $php_date_format = $this->getOption('php_date_format');
        $js_date_format = $this->getOption('js_date_format');
        $input_html_id = $this->generateId($name);
        $div_html_id = $input_html_id.'_div';
        $attributes['class'] = 'form-control';
        $attributes['data-format'] = $js_date_format;

        if (!empty($value)) {
            $value = date($php_date_format, strtotime($value));
        } else {
            $value = '';
        }

        $input = $this->renderTag('input', array_merge(
            array(
                'type' => 'text', 'id' => $input_html_id,
                'name' => $name, 'value' => $value
            ),
            $attributes
        ));

        return
            "
            <div id='$div_html_id' class='input-append date input-group col-sm-4 no-padding'>
                $input
                <span class='add-on input-group-addon'>
                    <i data-time-icon='icon-time' data-date-icon='icon-calendar'></i>
                </span>
            </div>
            <script type='text/javascript'>
              $(function() {
                $('#$div_html_id').datetimepicker({
                  language: 'en'
                });
              });
            </script>
            "
        ;
    }

    /**
     * Gets the stylesheet paths associated with the widget.
     *
     * @return array An array of stylesheet paths
     */
    public function getStylesheets()
    {
        return array('/aceAdminPlugin/css/bootstrap-datetimepicker.min.css' => 'all');
    }

    /**
     * Gets the JavaScript paths associated with the widget.
     *
     * @return array An array of JavaScript paths
     */
    public function getJavascripts()
    {
        return array(
            '/aceAdminPlugin/js/bootstrap-datetimepicker.min.js',
        );
    }
}
