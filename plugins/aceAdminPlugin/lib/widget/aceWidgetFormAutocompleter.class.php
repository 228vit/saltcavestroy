<?php

/*
 * This file is part of the symfony package.
 * (c) Fabien Potencier <fabien.potencier@symfony-project.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * aceWidgetFormAutocompleter represents an autocompleter input widget rendered by JQuery.
 *
 * This widget needs JQuery to work.
 *
 * You also need to include the JavaScripts and stylesheets files returned by the getJavaScripts()
 * and getStylesheets() methods.
 *
 * If you use symfony 1.2, it can be done automatically for you.
 *
 * @package    symfony
 * @subpackage widget
 * @author     Fabien Potencier <fabien.potencier@symfony-project.com>
 * @version    SVN: $Id: aceWidgetFormAutocompleter.class.php 15839 2009-02-27 05:40:57Z fabien $
 */
class aceWidgetFormAutocompleter extends sfWidgetFormInput
{
    /**
     * Configures the current widget.
     *
     * Available options:
     *
     *  * url:            The URL to call to get the choices to use (required)
     *  * config:         A JavaScript array that configures the JQuery autocompleter widget
     *  * value_callback: A callback that converts the value before it is displayed
     *
     * @param array $options     An array of options
     * @param array $attributes  An array of default HTML attributes
     *
     * @see sfWidgetForm
     */
    protected function configure($options = array(), $attributes = array())
    {
        $this->addOption('url', 'aceAdminAjax');
        $this->addOption('model', 'User');
        $this->addOption('addon', false);
        $this->addOption('addon-icon', 'user');
        $this->addOption('value_callback');
        $this->addOption('placeholder', 'type text here...');
        $this->addOption('config', '{ }');
        $this->addOption('input_attributes', []);
        $this->addOption('input_hidden_attributes', []);

        // this is required as it can be used as a renderer class for sfWidgetFormChoice
        $this->addOption('choices');

        parent::configure($options, $attributes);
    }

    /**
     * @param  string $name        The element name
     * @param  string $value       The date displayed in this widget
     * @param  array $attributes  An array of HTML attributes to be merged with the default HTML attributes
     * @param  array $errors      An array of errors for the field
     *
     * @return string An HTML tag string
     *
     * @see sfWidgetForm
     */
    public function render($name, $value = null, $attributes = array(), $errors = array())
    {
        $visibleValue = '';
        if ($this->getOption('value_callback')) {
            $visibleValue = call_user_func($this->getOption('value_callback'), $value);
        } else {
            // try to get value from Model
            if ($this->getOption('model')) {
                $model = $this->getOption('model').'_table';
                $model = sfInflector::camelize($model);

                try {
                    $res = $model::getInstance()->find($value);
                    if ($res) {
                        $visibleValue = $res->__toString();
                    }
                } catch (Exception $e) {
                    $visibleValue = $value;
                    // log::error($e->getCode(), $e->getMessage()); log::error($e->getCode(), $e->getTraceAsString());
                }
            }
        }

        $input_hidden_html_id = $this->generateId($name);
        $autocomplete_html_id = $this->generateId('autocomplete_' . $name);

        return
            $this->renderTag('input', array_merge(
                array(
                    'type' => 'hidden', 'id' => $input_hidden_html_id,
                    'name' => $name, 'value' => $value),
                $attributes
            ))
            .
            parent::render('autocomplete_' . $name, $visibleValue, $this->getOption('input_attributes'), $errors) .
            "
            <script>
                //autocomplete
                $('#{$autocomplete_html_id}').typeahead([{
                    remote: '".$this->getOption('url')."?q=%QUERY'
                }]).on('typeahead:selected', function (obj, datum) {
                    $('#{$input_hidden_html_id}').val(datum.id);
                    console.log(datum.value);
                    console.log(datum.id);
                });
                $('#{$autocomplete_html_id}').change(function(){
                    if ($(this).val() == '') {
                        $('#{$input_hidden_html_id}').val('');
                    }
                    console.log( $(this).val() );
                });
                console.log('#{$autocomplete_html_id}');

            </script>
            ";


        ;
    }

    /**
     * Gets the stylesheet paths associated with the widget.
     *
     * @return array An array of stylesheet paths
     */
    public function getStylesheets()
    {
        return [];
//        return array('/sfFormExtraPlugin/css/jquery.autocompleter.css' => 'all');
    }

    /**
     * Gets the JavaScript paths associated with the widget.
     *
     * @return array An array of JavaScript paths
     */
    public function getJavascripts()
    {
        return [];
//        return array('/sfFormExtraPlugin/js/jquery.autocompleter.js');
    }
}
