<?php

class aceWidgetFormPlainText extends sfWidgetForm
{
    /**
     * @param array $options An array of options
     * @param array $attributes An array of default HTML attributes
     *
     * @see sfWidgetForm
     */
    protected function configure($options = array(), $attributes = array())
    {
        $this->addOption('leading_zeros', false);
        $this->addOption('out_str_len', 0);
    }

    /**
     * @param  string $name The element name
     * @param  string $value The value displayed in this widget
     * @param  array $attributes An array of HTML attributes to be merged with the default HTML attributes
     * @param  array $errors An array of errors for the field
     *
     * @return string An HTML tag string
     *
     * @see sfWidgetForm
     */
    public function render($name, $value = null, $attributes = array(), $errors = array())
    {
        $options = $this->getOptions();
        if ($options['leading_zeros']) {
            $value = str_pad($value, $options['out_str_len'], '0', STR_PAD_LEFT);
        }
        return
            $this->renderTag('input', array_merge($attributes, array('type' => 'hidden', 'name' => $name, 'value' => $value))) .
            $this->renderContentTag('span', ' '.$value.' ', array('class' => 'badge badge-gray'));
    }
}