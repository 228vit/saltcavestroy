<?php

/*
 * This file is part of the symfony package.
 * (c) Fabien Potencier <fabien.potencier@symfony-project.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * sfWidgetFormInput represents an HTML text input tag.
 *
 * @package    symfony
 * @subpackage widget
 * @author     Fabien Potencier <fabien.potencier@symfony-project.com>
 * @version    SVN: $Id: sfWidgetFormInputText.class.php 30762 2010-08-25 12:33:33Z fabien $
 */
class aceWidgetFormInputRouble extends sfWidgetFormInput
{
    /**
     * Configures the current widget.
     *
     * @param array $options     An array of options
     * @param array $attributes  An array of default HTML attributes
     *
     * @see sfWidgetForm
     */
    protected function configure($options = array(), $attributes = array())
    {
        parent::configure($options, $attributes);

        $this->setOption('type', 'text');
    }

    /**
     * @param  string $name        The element name
     * @param  string $value       The date displayed in this widget
     * @param  array $attributes  An array of HTML attributes to be merged with the default HTML attributes
     * @param  array $errors      An array of errors for the field
     *
     * @return string An HTML tag string
     *
     * @see sfWidgetForm
     */
    public function render($name, $value = null, $attributes = array(), $errors = array())
    {

        $html_id = $this->generateId($name);
        $rouble_html_id = $this->generateId('rouble_'.$name);

        $attributes['class'] = 'form-control ';

        return "
        <div class='input-group'>
            "
            .
            $this->renderTag('input', array_merge(
                array(
                    'type' => 'text', 'id' => $html_id,
                    'name' => $name, 'value' => $value),
                $attributes
            ))
            .
            "
            <span class='input-group-btn'>
                <span class='input-group-addon' style='width: 150px; overflow: hidden;'>
                    <i class='icon-money bigger-110'></i> <span id='{$rouble_html_id}'>0</span></span>
            </span>
        </div>
        <script>
            $(function() {
                $('#{$html_id}').on('input', function() {
                    val = parseInt($(this).val()) / 100;
                    $('#{$rouble_html_id}').html(val);
                });
                val = parseInt($('#{$html_id}').val()) / 100;
                $('#{$rouble_html_id}').html(val);
            });
        </script>
        ";

    }
}
