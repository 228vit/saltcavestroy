<?php

/**
 * Bootstrap RichEditor
 *
 * This widget needs JQuery to work.
 *
 * You also need to include the JavaScripts and stylesheets files returned by the getJavaScripts()
 * and getStylesheets() methods.
 *
 */
class aceWidgetFormRichEditor extends sfWidgetFormInput
{
    /**
     * Configures the current widget.
     *
     * Available options:
     *
     *  * url:            The URL to call to get the choices to use (required)
     *  * config:         A JavaScript array that configures the JQuery autocompleter widget
     *  * value_callback: A callback that converts the value before it is displayed
     *
     * @param array $options     An array of options
     * @param array $attributes  An array of default HTML attributes
     *
     * @see sfWidgetForm
     */
    protected function configure($options = array(), $attributes = array())
    {
        $this->addOption('addon', false);
        $this->addOption('addon-icon', 'user');
        $this->addOption('value_callback');
        $this->addOption('placeholder', 'type text here...');
        $this->addOption('config', '{ }');
        $this->addOption('input_attributes', []);
        $this->addOption('input_hidden_attributes', []);

        // this is required as it can be used as a renderer class for sfWidgetFormChoice
        $this->addOption('choices');

        parent::configure($options, $attributes);
    }

    /**
     * @param  string $name        The element name
     * @param  string $value       The date displayed in this widget
     * @param  array $attributes  An array of HTML attributes to be merged with the default HTML attributes
     * @param  array $errors      An array of errors for the field
     *
     * @return string An HTML tag string
     *
     * @see sfWidgetForm
     */
    public function render($name, $value = null, $attributes = array(), $errors = array())
    {
        $html_id = $this->generateId($name);

$HTML = <<<EOT
<textarea name="{$name}" id="{$html_id}" rows="4" cols="20">
<script type="text/javascript">
    $(document).ready(function () {
        $('#{$html_id}').liveEdit({
            height: 350,
            css: ['/aceAdminPlugin/css/bootstrap.min.css', '/css/bootstrap_extend.css'] /* Apply bootstrap css into the editing area */,
            groups: [
                    ["group1", "", ["Bold", "Italic", "Underline", "ForeColor", "RemoveFormat"]],
                    ["group2", "", ["Bullets", "Numbering", "Indent", "Outdent"]],
                    ["group3", "", ["Paragraph", "FontSize", "FontDialog", "TextDialog"]],
                    ["group4", "", ["LinkDialog", "ImageDialog", "TableDialog", "Emoticons", "Snippets"]],
                    ["group5", "", ["Undo", "Redo", "FullScreen", "SourceDialog"]]
                    ] /* Toolbar configuration */
        });

        $('#{$html_id}').data('liveEdit').startedit();
    });


    function save() {
        var sHtml = $('#{$html_id}').data('liveEdit').getXHTMLBody(); //Use before finishedit()
        console.log(sHtml); /*You can use the sHtml for any purpose, eg. saving the content to your database, etc, depend on you custom app */
    }
</script>
</textarea>
EOT;
        
        return $HTML;
    }

    /**
     * Gets the stylesheet paths associated with the widget.
     *
     * @return array An array of stylesheet paths
     */
    public function getStylesheets()
    {
        return [];
//        return array('/sfFormExtraPlugin/css/jquery.autocompleter.css' => 'all');
    }

    /**
     * Gets the JavaScript paths associated with the widget.
     *
     * @return array An array of JavaScript paths
     */
    public function getJavascripts()
    {
        return [
            'innovaeditor.js',
            'innovamanager.js',
        ];
//        return array('/sfFormExtraPlugin/js/jquery.autocompleter.js');
    }
}
