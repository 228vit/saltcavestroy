<?php
require_once(dirname(__FILE__).'/../lib/BaseaceAdminDashComponents.class.php');

/**
 * aceAdminDash components.
 *
 * @package    plugins
 * @subpackage aceAdminDash
 * @author     kevin
 * @version    SVN: $Id: components.class.php 25203 2009-12-10 16:50:26Z Crafty_Shadow $
 */ 
class aceAdminDashComponents extends BaseaceAdminDashComponents
{
}