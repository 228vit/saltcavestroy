<?php

/**
 * aceAdminDash actions.
 *
 * @package    plugins
 * @subpackage aceAdminDash
 * @author     kevin
 * @version    SVN: $Id: actions.class.php 25203 2009-12-10 16:50:26Z Crafty_Shadow $
 */
class aceAdminAjaxActions extends sfActions
{
    /**
     * Get Model
     *
     * @param sfWebRequest $request
     * @return JSON
     */
    public function executeAjax(sfWebRequest $request)
    {
        $model = $request->getParameter('model', false);
        if (!$model) {
            return $this->renderText(json_encode(false));
        }
        $this->getResponse()->setContentType('application/json');
        $rows = Doctrine_Query::create()
            ->select('p.id, p.phone_number')
            ->from('Payer p')
            ->where('p.phone_number ILIKE ?', '%'.$request->getParameter('q', 0).'%')
            ->limit(20)
            ->fetchArray()
        ;

        $res = [];

        foreach ($rows as $p) {
            $res[] = [
                'value' => $p['phone_number'],
                'id' => $p['id']
            ];
        }
        return $this->renderText(json_encode($res));
    }

}
