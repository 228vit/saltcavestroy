<?php
  use_helper('I18N');
  /** @var Array of menu items */ $items = $sf_data->getRaw('items');
  /** @var Array of categories, each containing an array of menu items and settings */ $categories = $sf_data->getRaw('categories');
?>


<?php if (count($items)): ?>

<ul id="nav">
	<?php if (aceAdminDash::hasItemsMenu($items)): ?>
		<li><a href="#">Меню</a>
		
			<ul>
        		<?php include_partial('aceAdminDash/menu_list', array('items' => $items, 'items_in_menu' => true)); ?>
      		</ul>
		
		</li>
	<?php endif; ?>
	<?php include_partial('aceAdminDash/menu_list', array('items' => $items, 'items_in_menu' => false)); ?>

</ul>

<?php endif; ?>

<?php if (count($categories)): ?>
  <ul id="nav">
    <?php foreach ($categories as $name => $category): ?>
    <?php   if (aceAdminDash::hasPermission($category, $sf_user)): ?>
    <?php     if (aceAdminDash::hasItemsMenu($category['items'])): ?>
    <li class="active"><a href="#"><?php echo isset($category['name']) ? $category['name'] : $name ?></a>
      <ul>
        <?php include_partial('aceAdminDash/menu_list', array('items' => $category['items'], 'items_in_menu' => true)) ?>
      </ul>
    </li>
    <?php     endif; ?>
    <?php   endif; ?>
    <?php endforeach; ?>
    <?php foreach ($categories as $name => $category): ?>
        <?php include_partial('aceAdminDash/menu_list', array('items' => $category['items'], 'items_in_menu' => false)) ?>
    <?php endforeach; ?>
  </ul>
<?php elseif (!count($items)): ?>
  <?php echo __('aceAdminDashPlugin is not configured.  Please see the %documentation_link%.', array('%documentation_link%'=>link_to(__('documentation', null, 'sf_admin_dash'), 'http://www.symfony-project.org/plugins/aceAdminDashPlugin?tab=plugin_readme', array('title' => __('documentation', null, 'sf_admin_dash')))), 'sf_admin_dash'); ?>
<?php endif; ?>