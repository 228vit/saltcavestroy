<?php if ($sf_user->hasFlash('notice')): ?>
<div class="alert alert-block alert-success">
    <button data-dismiss="alert" class="close" type="button">
        <i class="icon-remove"></i>
    </button>

    <i class="icon-ok"></i>
    <?php echo __($sf_user->getFlash('notice'), array(), 'sf_admin') ?>
</div>
<?php endif; ?>

<?php if ($sf_user->hasFlash('error')): ?>
<div class="alert alert-danger">
    <button data-dismiss="alert" class="close" type="button">
        <i class="icon-remove"></i>
    </button>

    <i class="icon-alert"></i>
    <?php echo __($sf_user->getFlash('error'), array(), 'sf_admin') ?>
</div>
<?php endif; ?>
