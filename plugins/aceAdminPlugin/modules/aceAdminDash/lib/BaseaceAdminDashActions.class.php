<?php
/**
 * aceAdminDash base actions.
 *
 * @package    plugins
 * @subpackage aceAdminDash
 * @author     Ivan Tanev aka Crafty_Shadow @ webworld.bg <vankata.t@gmail.com>
 * @version    SVN: $Id: BaseaceAdminDashActions.class.php 25203 2009-12-10 16:50:26Z Crafty_Shadow $
 */ 
class BaseaceAdminDashActions extends aceActions
{
 
 /**
  * Executes the index action, which shows a list of all available modules
  *
  */
  public function executeDashboard()
  {    
    $this->items = aceAdminDash::getItems();

    $this->categories = aceAdminDash::getCategories();    
  } 
  
}