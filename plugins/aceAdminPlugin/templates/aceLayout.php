<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta name="description" content="overview &amp; stats"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <?php include_http_metas() ?>
    <?php include_metas() ?>
    <?php include_title() ?>

    <link rel="shortcut icon" href="/favicon.png"/>

    <?php include_stylesheets() ?>
    <?php include_javascripts() ?>

</head>

<body>
<div class="navbar navbar-default" id="navbar">
    <script type="text/javascript">
        try {
            ace.settings.check('navbar', 'float')
        } catch (e) {
        }
    </script>

    <?php include_partial('global/topNavigation') ?>

</div><!-- div class="navbar navbar-default" id="navbar" -->

<div class="main-container" id="main-container">
    <script type="text/javascript">
        try {
            ace.settings.check('main-container', 'float')
        } catch (e) {
        }
    </script>

    <div class="main-container-inner">
        <a class="menu-toggler" id="menu-toggler" href="#">
            <span class="menu-text"></span>
        </a>

        <div class="sidebar" id="sidebar">
            <script type="text/javascript">
                try {
                    ace.settings.check('sidebar', 'float')
                } catch (e) {
                }
            </script>

            <?php include_partial('global/favorites') ?>
            <!-- #sidebar-shortcuts -->

            <?php include_partial('global/leftMenu') ?>
            <!-- /.nav-list -->

            <div class="sidebar-collapse" id="sidebar-collapse">
                <i class="icon-double-angle-left" data-icon1="icon-double-angle-left"
                   data-icon2="icon-double-angle-right"></i>
            </div>

            <script type="text/javascript">
                try {
                    ace.settings.check('sidebar', 'collapsed')
                } catch (e) {
                }
            </script>
        </div>
        <!-- /.sidebar -->

        <div class="main-content" id="mainContent">

            <div class="breadcrumbs" id="breadcrumbs">
                <?php include_partial('global/breadcrumb') ?>

                <script type="text/javascript">
                    $(function () {

                        $(".popupSelectedRecord").click(function(){
                            listbox_id = '#'+$(this).attr('rel')
                            selected_id = $(listbox_id + ' option:selected').val()
                            url = $(this).attr('url') + '/' + selected_id

                            console.log('listbox_id: ' + listbox_id + ' selected_id: ' + selected_id + ' url: ' + url)

                            var dialog = $('#dialog');
                            if ($('#dialog').length == 0) {
                                dialog = $('<div id=\"{$popup_html_id}\" style=\"display:hidden;\"></div>').appendTo('body');
                            }

                            // load remote content
                            dialog.load(
                                url,
                                //{},
                                function(responseText, textStatus, XMLHttpRequest) {
                                    dialog.dialog({
                                        modal: true,
                                        title: 'Information',
                                        title_html: true,
                                        width:'auto',
                                        buttons: [
                                            {
                                                text: 'OK',
                                                'class' : 'btn btn-primary btn-xs',
                                                click: function() {
                                                    $( this ).dialog( 'close' );
                                                }
                                            }
                                        ]
                                    });
                                }
                            );

                            //prevent the browser to follow the link
                            return false;
                        })

                        $('a.ajax_confirm').click(function () {
                            if (!confirm('Are you sure?')) {
                                return false;
                            }
                            var url = this.href;
                            var dialog = $("#dialogInner");
                            if ($("#dialogInner").length == 0) {
                                dialog = $('<div id="dialogInner" style="display:hidden;"></div>').appendTo('body');
                            }

                            // load remote content
                            dialog.load(
                                url,
                                //{},
                                function (responseText, textStatus, XMLHttpRequest) {
                                    dialog.dialog({
                                        modal: true,
                                        title: "Information",
                                        title_html: true,
                                        width: 'auto',
                                        buttons: [
                                            {
                                                text: "OK",
                                                "class": "btn btn-primary btn-xs",
                                                click: function () {
                                                    $(this).dialog("close");
                                                }
                                            }
                                        ]
                                    });
                                }
                            );
                            //prevent the browser to follow the link
                            return false;
                        });

                        function initAjaxPopups() {
                            $('a.ajax').click(function() {
                                var url = this.href;
                                var dialog = $("#dialog");
                                if ($("#dialog").length == 0) {
                                    dialog = $('<div id="dialog" style="display:hidden;"></div>').appendTo('body');
                                }

                                // load remote content
                                dialog.load(
                                    url,
                                    //{},
                                    function(responseText, textStatus, XMLHttpRequest) {
                                        dialog.dialog({
                                            modal: true,
                                            title: "Information",
                                            title_html: true,
                                            width:'auto',
                                            buttons: [
                                                {
                                                    text: "OK",
                                                    "class" : "btn btn-primary btn-xs",
                                                    click: function() {
                                                        $( this ).dialog( "close" );
                                                    }
                                                }
                                            ]
                                        });
                                    }
                                );
                                //prevent the browser to follow the link
                                return false;
                            });
                        }

                        initAjaxPopups();
                    });
                </script>
            </div>

            <div class="page-content">

                <?php echo $sf_content ?>

            </div>
            <!-- /.page-content -->

        </div>
        <!-- /.main-content -->

        <?php if (has_slot('filter_form')): ?>
            <?php include_slot('filter_form') ?>
        <?php endif; ?>
    </div>
    <!-- /.main-container-inner -->

    <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
        <i class="icon-double-angle-up icon-only bigger-110"></i>
    </a>

</div>
<!-- /.main-container -->

<!-- Modal -->
<div class="modal fade" id="popupModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- basic scripts -->

<!--[if !IE]> -->

<script type="text/javascript">
    window.jQuery || document.write("<script src='/aceAdminPlugin/js/jquery-2.0.3.min.js'>" + "<" + "/script>");
</script>

<!-- <![endif]-->

<!--[if IE]>
<script type="text/javascript">
    window.jQuery || document.write("<script src='/aceAdminPlugin/js/jquery-1.10.2.min.js'>" + "<" + "/script>");
</script>
<![endif]-->

<script type="text/javascript">
    if ("ontouchend" in document) document.write("<script src='/aceAdminPlugin/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
</script>
<script src="/aceAdminPlugin/js/bootstrap.min.js"></script>
<script src="/aceAdminPlugin/js/typeahead-bs2.min.js"></script>

<!-- page specific plugin scripts -->

<!--[if lte IE 8]>
<script src="/aceAdminPlugin/js/excanvas.min.js"></script>
<![endif]-->

<script src="/aceAdminPlugin/js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="/aceAdminPlugin/js/jquery.ui.touch-punch.min.js"></script>
<script src="/aceAdminPlugin/js/jquery.slimscroll.min.js"></script>
<script src="/aceAdminPlugin/js/jquery.easy-pie-chart.min.js"></script>
<script src="/aceAdminPlugin/js/jquery.sparkline.min.js"></script>
<script src="/aceAdminPlugin/js/flot/jquery.flot.min.js"></script>
<script src="/aceAdminPlugin/js/flot/jquery.flot.pie.min.js"></script>
<!--<script src="/aceAdminPlugin/js/flot/jquery.flot.resize.min.js"></script>-->
<script src="/aceAdminPlugin/js/bootbox.min.js"></script>
<script src="/aceAdminPlugin/js/date-time/daterangepicker.min.js"></script>
<!-- ace scripts -->

<script src="/aceAdminPlugin/js/ace-elements.min.js"></script>
<script src="/aceAdminPlugin/js/ace.min.js"></script>

<!-- inline scripts related to this page -->
<script src="/aceAdminPlugin/js/dashboard_javascript.js"></script>

<script>
    jQuery(document).ready(function ($) {

        $("[data-rel=tooltip]").tooltip();

    });
</script>

</body>
</html>
