<?php /* @var $sf_user sfGaurdUser */ ?>

<div class="navbar-container" id="navbar-container">
    <div class="navbar-header pull-left">
        <a class="navbar-brand" href="/">
            <small>
                <i class="icon-cog"></i>
                <?php echo sfConfig::get('app_site_name', 'Control panel') ?>
            </small>
        </a><!-- /.brand -->
    </div>
    <!-- /.navbar-header -->

    <div class="navbar-header pull-right" role="navigation">
        <ul class="nav ace-nav">

            <li id="ticketsCounter" class="light-blue">
                <!-- get content via AJAX -->
            </li>

            <li id="ticketUnreadMessagesCounter" class="light-blue">
                <!-- get content via AJAX -->
            </li>



            <li class="light-blue">
                <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                    <i class="icon-tasks"></i>
                    <span class="badge badge-grey">4</span>
                </a>

                <ul class="pull-right dropdown-navbar dropdown-menu dropdown-caret dropdown-close">
                    <li class="dropdown-header">
                        <i class="icon-ok"></i>
                        4 Tasks to complete
                    </li>

                    <li>
                        <a href="#">
                            <div class="clearfix">
                                <span class="pull-left">Software Update</span>
                                <span class="pull-right">65%</span>
                            </div>

                            <div class="progress progress-mini ">
                                <div style="width:65%" class="progress-bar "></div>
                            </div>
                        </a>
                    </li>

                    <li>
                        <a href="#">
                            <div class="clearfix">
                                <span class="pull-left">Hardware Upgrade</span>
                                <span class="pull-right">35%</span>
                            </div>

                            <div class="progress progress-mini ">
                                <div style="width:35%" class="progress-bar progress-bar-danger"></div>
                            </div>
                        </a>
                    </li>

                    <li>
                        <a href="#">
                            <div class="clearfix">
                                <span class="pull-left">Unit Testing</span>
                                <span class="pull-right">15%</span>
                            </div>

                            <div class="progress progress-mini ">
                                <div style="width:15%" class="progress-bar progress-bar-warning"></div>
                            </div>
                        </a>
                    </li>

                    <li>
                        <a href="#">
                            <div class="clearfix">
                                <span class="pull-left">Bug Fixes</span>
                                <span class="pull-right">90%</span>
                            </div>

                            <div class="progress progress-mini progress-striped active">
                                <div style="width:90%" class="progress-bar progress-bar-success"></div>
                            </div>
                        </a>
                    </li>

                    <li>
                        <a href="#">
                            See tasks with details
                            <i class="icon-arrow-right"></i>
                        </a>
                    </li>
                </ul>
            </li>


            <li class="light-blue">
                <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                    <img class="nav-user-photo" src="/assets/avatars/avatar.png" alt="User Photo"/>
								<span class="user-info">
									<small>Welcome,</small>
                                    <?php echo $sf_user->getGuardUser()->getUsername() ?>
								</span>

                    <i class="icon-caret-down"></i>
                </a>

                <ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                    <li>
                        <a href="#">
                            <i class="icon-cog"></i>
                            Settings
                        </a>
                    </li>

                    <li>
                        <a href="#">
                            <i class="icon-user"></i>
                            Profile
                        </a>
                    </li>

                    <li class="divider"></li>

                    <li>
                        <a href="<?php echo url_for('@sf_guard_signout') ?>">
                            <i class="icon-off"></i>
                            Logout
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
        <!-- /.ace-nav -->
    </div>
    <!-- /.navbar-header -->
</div>
<!-- /.container -->
