<script type="text/javascript">
    try {
        ace.settings.check('breadcrumbs', 'fixed')
    } catch (e) {
    }
</script>

<?php if (has_slot('breadcrumbs')): ?>
    <?php include_slot('breadcrumbs') ?>
<?php else: ?>

<ul class="breadcrumb">
    <li>
        <i class="icon-home home-icon"></i>
        <a href="#">Home</a>
    </li>
    <li class="active">Dashboard</li>
</ul>
<!-- .breadcrumb -->

<?php endif; ?>