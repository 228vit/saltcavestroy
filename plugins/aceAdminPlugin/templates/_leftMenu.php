<?php
use_helper('I18N');
/** @var Array of menu items */
$items = aceAdminDash::getItems();
//$items = $sf_data->getRaw('items');
/** @var Array of categories, each containing an array of menu items and settings */
$categories = aceAdminDash::getCategories();
//print_r($categories); die();
//$categories = $sf_data->getRaw('categories');
?>

<ul class="nav nav-list">
    <li>
        <a href="/">
            <i class="icon-dashboard"></i>
            <span class="menu-text"> Dashboard </span>
        </a>
    </li>

    <?php if (count($categories)): ?>

        <?php foreach ($sf_user->getPermissionNames() as $name): ?>
            <?php // echo '<li>'.$name ?>
        <?php endforeach; // ($sf_user->get as $name => $category): ?>

        <?php foreach ($sf_user->getCredentials() as $name): ?>
        <?php endforeach; // ($sf_user->get as $name => $category): ?>

        <?php foreach ($categories as $name => $category): ?>

            <?php if (aceAdminDash::hasPermission($category, $sf_user)): ?>

                <?php if (aceAdminDash::hasItemsMenu($category['items'])): ?>

                    <li class="" id="menu_<?php $cat_id = isSet($category['id']) ? $category['id'] : rand(1111, 9999); echo $cat_id ?>">
                        <a class="dropdown-toggle" href="#">
                            <i class="icon-<?php echo isSet($category['icon']) ? $category['icon'] : 'plus' ?>"></i>
                            <span class="menu-text">
                                <?php echo $name ?>
                            </span>
                            <b class="arrow icon-angle-down"></b>
                        </a>

                        <ul class="submenu">
                            <?php foreach ($category['items'] as $itemName => $item): ?>


                                <?php if (isSet($item['items'])): ?>
                                    <li class=""
                                        id="menu_<?php $sub_cat_id = isSet($item['id']) ? $item['id'] : rand(1111, 9999); echo $sub_cat_id ?>">
                                        <a class="dropdown-toggle" href="#">
                                            <i class="icon-<?php echo isSet($item['icon']) ? $item['icon'] : 'plus' ?>"></i>
                                        <span class="menu-text">
                                            <?php echo $itemName ?>
                                        </span>
                                            <b class="arrow icon-angle-down"></b>
                                        </a>

                                        <ul class="submenu">
                                            <?php foreach ($item['items'] as $itemName2 => $item2): ?>
                                                <?php if (aceAdminDash::hasPermission($item2, $sf_user)): ?>
                                                    <?php $is_current_module = ($sf_context->getModuleName() == $item2['url']) ? true : false; ?>
                                                    <?php if ($is_current_module): ?>
                                                        <script>
                                                            $('#menu_<?php echo $cat_id ?>').addClass('active open')
                                                            $('#menu_<?php echo $sub_cat_id ?>').addClass('active open')
                                                        </script>
                                                    <?php endif; ?>
                                                    <li class="<?php echo $is_current_module ? 'active' : '' ?>">
                                                        <a href="<?php echo url_for($item2['url']) ?>">
                                                            <?php // echo $is_current_module ? '<i class="icon-double-angle-right"></i>' : '' ?>
                                                            <?php echo $itemName2 ?>
                                                        </a>
                                                    </li>
                                                <?php endif; ?>

                                            <?php endforeach; ?>
                                        </ul>
                                    </li>

                                <?php else: ?>

                                    <?php if (aceAdminDash::hasPermission($item, $sf_user)): ?>
                                        <?php $is_current_module = isSet($item['url']) && ($sf_context->getModuleName() == $item['url']) ? true : false; ?>
                                        <?php if ($is_current_module): ?>
                                            <script>
                                                $('#menu_<?php echo $cat_id ?>').addClass('active open')
                                            </script>
                                        <?php endif; ?>
                                        <li class="<?php echo $is_current_module ? 'active' : '' ?>">
                                            <a href="<?php echo url_for($item['url']) ?>">
                                                <?php echo $is_current_module ? '<i class="icon-double-angle-right"></i>' : '' ?>
                                                <?php echo $itemName ?>
                                            </a>
                                        </li>
                                    <?php endif; ?>

                                <?php endif; ?>

                            <?php endforeach; ?>

                        </ul>

                        <?php // include_partial('aceAdminDash/menu_list', array('items' => $category['items'], 'items_in_menu' => false)) ?>
                    </li>

                <?php endif; ?>

            <?php endif; ?>

        <?php endforeach; ?>

    <?php elseif (!count($items)): ?>
        <?php echo __('aceAdminDashPlugin is not configured.  Please see the %documentation_link%.', array('%documentation_link%' => link_to(__('documentation', null, 'sf_admin_dash'), 'http://www.symfony-project.org/plugins/aceAdminDashPlugin?tab=plugin_readme', array('title' => __('documentation', null, 'sf_admin_dash')))), 'sf_admin_dash'); ?>
    <?php endif; ?>

</ul>

<script>
    $('.ajaxCall').click(function () {

        console.log('me ' + $(this).html());
        $(this).parent().siblings().each(function () {
            console.log('parent ' + $(this).tagName + ' // ' + $(this).html());
        });

        // remove ".active" from all menu items
        $('ul.nav li.link').removeClass('active');
        // set me as ".active"
        $(this).parent().addClass('active');

        // make ajax call
        url = $(this).attr("href");
        $.ajax({
            url: url,
            context: document.body
        }).done(function (resHtml) {
                $('#mainContent').html(resHtml);
            });

        // return false to prevent form submission
        return false;
    })


</script>