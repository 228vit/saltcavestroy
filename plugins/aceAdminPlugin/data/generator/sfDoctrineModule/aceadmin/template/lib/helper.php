[?php

/**
 * <?php echo $this->getModuleName() ?> module configuration.
 *
 * @package    ##PROJECT_NAME##
 * @subpackage <?php echo $this->getModuleName()."\n" ?>
 * @author     ##AUTHOR_NAME##
 * @version    SVN: $Id: helper.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class Base<?php echo ucfirst($this->getModuleName()) ?>GeneratorHelper extends aceAdminModelGeneratorHelper
{
    public function getUrlForAction($action)
    {
        return 'list' == $action ? '<?php echo $this->params['route_prefix'] ?>' : '<?php echo $this->params['route_prefix'] ?>_'.$action;
    }

    public function linkToFoo($name, $params)
    {
        return '<li class="sf_admin_action_save_and_list">foo '.$name.'</li>';
    }

    public function getLinkToAction($action, $params, $pk_link = false)
	{

	    $action = isset($params['action']) ? $params['action'] : 'List'.sfInflector::camelize($action);

        $icon = isSet($params['icon']) ? $params['icon'] : 'info';
        $class = isSet($params['class']) ? $params['class'] : 'green';
        $label = isSet($params['label']) ? $params['label'] : 'tooltip';
        $popup = isSet($params['popup']) ? $params['popup'] : false;
        $ajax = isSet($params['ajax']) ? $params['ajax'] : false;
        $ajax_confirm = isSet($params['ajax_confirm']) ? $params['ajax_confirm'] : false;

        if ($ajax) { $class .= " ajax"; }
        if ($ajax_confirm) { $class .= " ajax_confirm"; }

        $link_params = [
            'data-rel' => 'tooltip',
            'title' => $label,
            'data-original-title' => 'Default tooltip',
            'class' => $class,
            'confirm' => !empty($params['confirm']) ? $params['confirm'] : false
        ];

        if ($popup) {
            $link_params['data-toggle'] = 'modal';
            $link_params['data-target'] = '#popupModal';
        }

        $url_params = $pk_link ? '?'.$this->getPrimaryKeyUrlParams() : '\'';

        return '[?php echo link_to(__(\'wtf '.$params['label'].'\', array(), \''.$this->getI18nCatalogue().'\'), \''.$this->getModuleName().'/'.$action.$url_params.', '.$this->asPhp($params['params']).') ?]';

        return
            link_to(
                "<i class='icon-{$icon}' bigger-130' data-rel='tooltip' data-original-title='{$label}'></i>",
                $this->getModuleName().'/'.$action.$url_params,
                $link_params
            )
        ;

	}


    public function linkToList($params)
    {
        return '<li class="sf_admin_action_list">' . link_to(__($params['label'], array(), 'sf_admin'), '@' . $this->getUrlForAction('list')) . '</li>';
    }

    public function linkToListAsModel($params)
    {
        return '<li class="sf_admin_action_list">' . link_to($this->getModuleName(), '@' . $this->getUrlForAction('list')) . '</li>';
    }

    public function linkToAction($action, $object, $params, $pk_link = false)
    {
        $icon = isSet($params['icon']) ? $params['icon'] : 'info';
        $class = isSet($params['class']) ? $params['class'] : 'green';
        $label = isSet($params['label']) ? $params['label'] : 'tooltip';
        $popup = isSet($params['popup']) ? $params['popup'] : false;
        $ajax = isSet($params['ajax']) ? $params['ajax'] : false;
        $ajax_confirm = isSet($params['ajax_confirm']) ? $params['ajax_confirm'] : false;

        $actionName = 'List'.sfInflector::camelize($action);
        $url = url_for('<?php echo $this->params['route_prefix'] ?>'.'/'.$actionName.'?id='.$object->id);

        if ($ajax) { $class .= " ajax"; }
        if ($ajax_confirm) { $class .= " ajax_confirm"; }

        $link_params = [
            'data-rel' => 'tooltip',
            'title' => $label,
            'data-original-title' => 'Default tooltip',
            'class' => $class,
            'confirm' => !empty($params['confirm']) ? $params['confirm'] : false
        ];

        if ($popup) {
            $link_params['data-toggle'] = 'modal';
            $link_params['data-target'] = '#popupModal';
        }

        return link_to(
            '<i class="icon-'.$icon.' bigger-130" data-rel="tooltip" data-original-title="'.$label.'"></i>',
            $url,
            $link_params
        );
    }

    public function linkToEdit($object, $params)
    {
        return link_to(
            '<i class="icon-pencil bigger-130"></i>',
            $this->getUrlForAction('edit'),
            $object,
            array(
                'class' => 'green',
                'title' => 'Edit record',
            ));
    }

    public function linkToDelete($object, $params)
    {
        if ($object->isNew()) {
            return '';
        }

        return link_to(
            '<i class="icon-trash bigger-130"></i>',
            $this->getUrlForAction('delete'),
            $object,
            array(
                'class' => 'red',
                'title' => 'Delete record',
                'method' => 'delete',
                'confirm' => !empty($params['confirm']) ? __($params['confirm'], array(), 'sf_admin') : $params['confirm']
            )
        );
    }

    public function btnToAction($action, $object, $params, $pk_link = false)
    {
        $icon = isSet($params['icon']) ? $params['icon'] : 'info';
        $class = isSet($params['class']) ? $params['class'] : 'success';
        $label = isSet($params['label']) ? $params['label'] : 'tooltip';
        $popup = isSet($params['popup']) ? $params['popup'] : false;
        $ajax = isSet($params['ajax']) ? $params['ajax'] : false;
        $ajax_confirm = isSet($params['ajax_confirm']) ? $params['ajax_confirm'] : false;

        $actionName = 'List'.sfInflector::camelize($action);
        $url = url_for('<?php echo $this->params['route_prefix'] ?>'.'/'.$actionName.'?id='.$object->id);

        if ($ajax) { $class .= " ajax"; }
        if ($ajax_confirm) { $class .= " ajax_confirm"; }
        $link_params = [
            'data-rel' => 'tooltip',
            'title' => $label,
            'data-original-title' => 'Default tooltip',
            'class' => "btn btn-{$class}",
            'confirm' => !empty($params['confirm']) ? $params['confirm'] : false
        ];

        if ($popup) {
            $link_params['data-toggle'] = 'modal';
            $link_params['data-target'] = '#popupModal';
        }

        return link_to(
            '<i class="icon-'.$icon.' bigger-130" data-rel="tooltip" data-original-title="'.$label.'"></i> '.$label,
            $url,
            $link_params
        );
    }


    public function btnLink($params)
    {
        $icon = isSet($params['icon']) ? $params['icon'] : 'info';
        $class = isSet($params['class']) ? $params['class'] : 'success';
        return link_to(
            '<i class="icon-'.$icon.' bigger-130"></i> '.$params['label'],
            $this->getUrlForAction('list'),
            array(),
            array(
                'class' => "btn btn-{$class}",
                'confirm' => !empty($params['confirm']) ? $params['confirm'] : false
            )
        );
    }

    public function btnLinkToNew($params)
    {
        return link_to(
            '<i class="icon-plus bigger-130"></i> Add new',
            $this->getUrlForAction('new'),
            array(),
            array('class' => 'btn btn-success')
        );
    }

    public function btnToSave($params)
    {
        return '
            <button class="btn btn-info" type="submit">
                <i class="icon-ok bigger-110"></i>
                Submit
            </button>
        ';

    }

    public function btnToList($params)
    {
        return link_to(
            '<i class="icon-list bigger-130"></i> Back to list',
            $this->getUrlForAction('list'),
            array(),
            array('class' => 'btn btn-ok ')
        );

        return '
            <button class="btn btn-info" type="submit">
                <i class="icon-list bigger-110"></i>
                Back to list
            </button>
        ';

    }

    public function btnLinkToList($params)
    {
        return link_to(
            '<i class="icon-list bigger-130"></i>',
            $this->getUrlForAction('list'),
            array('class' => 'green',
                'method' => 'delete',
                'confirm' => !empty($params['confirm']) ? __($params['confirm'], array(), 'sf_admin') : $params['confirm']
            )
        );
        return '
            <button class="btn btn-info" type="submit">
                <i class="icon-list bigger-110"></i>
                Back to list
            </button>
        ';
    }

    public function btnLinkToDelete($object, $params)
    {
        if ($object->isNew()) {
            return '';
        }

        return link_to(
            '<span class="icon-trash bigger-130"></span>'.$params['label'],
            $this->getUrlForAction('delete'),
            $object,
            array('class' => 'btn btn-danger ',
                'method' => 'delete',
                'confirm' => !empty($params['confirm']) ? __($params['confirm'], array(), 'sf_admin') : $params['confirm']
            )
        );
    }

    public function btnLinkToSaveAndAdd($object, $params)
    {
        if (!$object->isNew()) {
        return '';
        }

        return '
            <button class="btn btn-success" type="submit" name="_save_and_add">
                <i class="icon-save bigger-110"></i>
                Save and add
            </button>
        ';

        return '<li class="sf_admin_action_save_and_add"><input type="submit" value="' . __($params['label'], array(), 'sf_admin') . '" name="_save_and_add" /></li>';
    }

    public function btnLinkToSaveAndList($object, $params)
    {
        if (!$object->isNew()) {
        return '';
        }

        return '
            <button class="btn btn-success" type="submit" name="_save_and_add">
                <i class="icon-list-alt bigger-110"></i>
                Save and list
            </button>
        ';

        return '<li class="sf_admin_action_save_and_add"><input type="submit" value="' . __($params['label'], array(), 'sf_admin') . '" name="_save_and_list" /></li>';
    }

}
