<div class="clearfix form-actions">
    <div class="col-md-offset-0 col-md-9 pull-left">
        <?php foreach (array('new', 'edit') as $action): ?>
            <?php if ('new' == $action): ?>
                [?php if ($form->isNew()): ?]
            <?php else: ?>
                [?php else: // isNew ?]
            <?php endif; ?>
            <?php foreach ($this->configuration->getValue($action . '.actions') as $name => $params): ?>

                <?php if ('_delete' == $name): ?>
                    <?php echo $this->addCredentialCondition('[?php echo $helper->btnLinkToDelete($form->getObject(), ' . $this->asPhp($params) . ') ?]', $params) ?>

                <?php elseif ('_list' == $name): ?>
                    <?php echo $this->addCredentialCondition('[?php echo $helper->btnToList(' . $this->asPhp($params) . ') ?]', $params) ?>

                <?php elseif ('_save' == $name): ?>
                    <?php echo $this->addCredentialCondition('[?php echo $helper->btnToSave(' . $this->asPhp($params) . ') ?]', $params) ?>

                <?php elseif ('_save_and_add' == $name): ?>
                    <?php echo $this->addCredentialCondition('[?php echo $helper->btnLinkToSaveAndAdd($form->getObject(), ' . $this->asPhp($params) . ') ?]', $params) ?>

                <?php elseif ('_save_and_list' == $name): ?>
                    <?php echo $this->addCredentialCondition('[?php echo $helper->btnLinkToSaveAndList($form->getObject(), ' . $this->asPhp($params) . ') ?]', $params) ?>

                <?php elseif ('_new' == $name): ?>
                    <?php echo $this->addCredentialCondition('[?php echo $helper->linkToNew(' . $this->asPhp($params) . ') ?]', $params) . "\n" ?>

                <?php else: ?>
                    [?php if (method_exists($helper, 'linkTo<?php echo $method = ucfirst(sfInflector::camelize($name)) ?>')): ?]
                    <?php echo $this->addCredentialCondition('[?php echo $helper->linkTo' . $method . '($form->getObject(), ' . $this->asPhp($params) . ') ?]', $params) ?>

                    [?php else: // custom action doesnt exists ?]
                    <?php // echo $this->addCredentialCondition($this->getLinkToAction($name, $params, true), $params) ?>
                    <?php echo $this->addCredentialCondition('[?php echo $helper->btnToAction("'.$method.'", $form->getObject(), ' . $this->asPhp($params) . ') ?]', $params) ?>

                    [?php endif; // method exists ?]
                <?php endif; ?>
            <?php endforeach; ?>
        <?php endforeach; ?>
        [?php endif; ?]

    </div>

    <div class="col-md-2 pull-right">
        [?php if (!$form->isNew()): ?]
            [?php if (isSet($prev) && $prev): ?]
                [?php $button = '<span class="btn btn-success btn-prev"><i class="icon-arrow-left"></i> Prev</span>' ?]
            <?php echo $this->addCredentialCondition('[?php echo link_to($button, $helper->getUrlForAction("edit"), $prev) ?]', $params = array()) ?>
            [?php else: ?]
                <button class="btn btn-prev" disabled="disabled"><i class="icon-arrow-left"></i>Prev</button>
                <?php $button = '<button class="btn btn-prev" disabled="disabled"><i class="icon-arrow-left"></i>Prev</button>' ?>
            [?php endif; ?]

            <span>&nbsp;</span>

            [?php if (isSet($next) && $next): ?]
                [?php $button = '<span class="btn btn-success btn-next">Next<i class="icon-arrow-right icon-on-right"></i></span>' ?]
                <?php echo $this->addCredentialCondition('[?php echo link_to($button, $helper->getUrlForAction("edit"), $next) ?]', $params = array()) ?>
            [?php else: ?]
                <span class="btn btn-next" disabled="disabled">Next<i class="icon-arrow-right icon-on-right"></i></span>
            [?php endif; ?]
        [?php endif; ?]
    </div>
</div>
<br clear="all"/>