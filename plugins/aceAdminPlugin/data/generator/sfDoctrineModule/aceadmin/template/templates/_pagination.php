
<div class="dataTables_paginate paging_bootstrap sf_admin_pagination">
    [?php if ($pager->haveToPaginate()): ?]
        <ul class="pagination">
            <li>
                <a href="[?php echo url_for('@<?php echo $this->getUrlForAction('list') ?>') ?]?page=1">
                    &laquo;&laquo;
                </a>
            </li>

            <li>
                <a href="[?php echo url_for('@<?php echo $this->getUrlForAction('list') ?>') ?]?page=[?php echo $pager->getPreviousPage() ?]">
                    &laquo;
                </a>
            </li>


            [?php foreach ($pager->getLinks(sfConfig::get('app_pages_in_pager', 10)) as $page): ?]
            [?php if ($page == $pager->getPage()): ?]
                <li class="active"><a href="">[?php echo $page ?]</a></span></li>
            [?php else: ?]
                <li>
                    <a href="[?php echo url_for('@<?php echo $this->getUrlForAction('list') ?>') ?]?page=[?php echo $page ?]">[?php echo
                        $page ?]</a>
                </li>
            [?php endif; ?]
            [?php endforeach; ?]

            <li>
                <a href="[?php echo url_for('@<?php echo $this->getUrlForAction('list') ?>') ?]?page=[?php echo $pager->getNextPage() ?]">
                    &raquo;
                </a>
            </li>

            <li>
                <a href="[?php echo url_for('@<?php echo $this->getUrlForAction('list') ?>') ?]?page=[?php echo $pager->getLastPage() ?]">
                    &raquo;&raquo;
                </a>
            </li>

            <li>
                &nbsp;&nbsp;&nbsp;rows per page::
                <select name="max_per_page" id="selectMaxPerPage" onchange="changeFnc()">
                    <option value="10"
                    [?php echo $sf_user->getAttribute('max_per_page', 50) == '10' ? 'selected' : '' ?]>10</option>
                    <option value="20"
                    [?php echo $sf_user->getAttribute('max_per_page', 50) == '20' ? 'selected' : '' ?]>20</option>
                    <option value="50"
                    [?php echo $sf_user->getAttribute('max_per_page', 50) == '50' ? 'selected' : '' ?]>50</option>
                    <option value="100"
                    [?php echo $sf_user->getAttribute('max_per_page', 50) == '100' ? 'selected' : '' ?]>100</option>
                    <option value="200"
                    [?php echo $sf_user->getAttribute('max_per_page', 50) == '200' ? 'selected' : '' ?]>200</option>
                </select>
            </li>

            <li>
                &nbsp;&nbsp;&nbsp;go to page:
                <input class="input-sm target" type="text" name="page" size="2" id="goToPageNum"/>
                <input class="btn btn-xs btn-grey" type="button" value="ок" onclick="goToPage()"/>
            </li>
        </ul><!-- ul class="pagination" -->
    [?php endif; // if have to paginate ?]


</div>
<script>

    function changeFnc() {
        var e = document.getElementById("selectMaxPerPage");
        var mpp = e.options[e.selectedIndex].value;
        document.location = "[?php echo url_for('@<?php echo $this->getUrlForAction('list') ?>') ?]?page=1&max_per_page=" + mpp;
    }

    function goToPage() {
        document.location = "[?php echo url_for('@<?php echo $this->getUrlForAction('list') ?>') ?]?page=" + document.getElementById("goToPageNum").value;
    }
</script>