[?php use_helper('I18N', 'Date') ?]
[?php include_partial('<?php echo $this->getModuleName() ?>/assets') ?]
[?php include_partial('<?php echo $this->getModuleName() ?>/breadcrumbs') ?]

[?php slot('breadcrumbs') ?]
<ul class="breadcrumb">
    <li>
        <i class="icon-home home-icon"></i>
        <a href="#">Home</a>
    </li>
    <li class="">
        <a href="/">Dashboard</a>
    </li>
    <li class="">
        <a href="<?php echo $_SERVER['SCRIPT_NAME'].'/'. $this->getModuleName() ?>"><?php echo sfInflector::humanize($this->getModuleName()) ?></a>
    </li>
    <li class="active">
        new
    </li>
</ul>
[?php end_slot() ?]

<div class="page-header">
    <h1>[?php echo <?php echo $this->getI18NString('new.title') ?> ?]</h1>
</div>

<div id="widget-body">

  [?php include_partial('<?php echo $this->getModuleName() ?>/flashes') ?]

  <div id="sf_admin_header">
    [?php include_partial('<?php echo $this->getModuleName() ?>/form_header', array('<?php echo $this->getSingularName() ?>' => $<?php echo $this->getSingularName() ?>, 'form' => $form, 'configuration' => $configuration)) ?]
  </div>

  <div id="sf_admin_content">
    [?php include_partial('<?php echo $this->getModuleName() ?>/form', array('<?php echo $this->getSingularName() ?>' => $<?php echo $this->getSingularName() ?>, 'form' => $form, 'configuration' => $configuration, 'helper' => $helper, 'next' => $next, 'prev' => $prev)) ?]
  </div>

  <div id="sf_admin_footer">
    [?php include_partial('<?php echo $this->getModuleName() ?>/form_footer', array('<?php echo $this->getSingularName() ?>' => $<?php echo $this->getSingularName() ?>, 'form' => $form, 'configuration' => $configuration)) ?]
  </div>
</div>
