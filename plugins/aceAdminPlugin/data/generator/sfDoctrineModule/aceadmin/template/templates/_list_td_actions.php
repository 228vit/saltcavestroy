<td>
    <div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">
        <?php foreach ($this->configuration->getValue('list.object_actions') as $name => $params): ?>
            <?php if ('_delete' == $name): ?>

                <?php echo $this->addCredentialCondition('[?php echo $helper->linkToDelete($'.$this->getSingularName().', '.$this->asPhp($params).') ?]', $params) ?>

            <?php elseif ('_edit' == $name): ?>

                <?php echo $this->addCredentialCondition('[?php echo $helper->linkToEdit($'.$this->getSingularName().', '.$this->asPhp($params).') ?]', $params) ?>

            <?php else: ?>
                <?php // echo $name.'/'.$this->getSingularName() ?>
                <?php echo $this->addCredentialCondition('[?php echo $helper->linkToAction(\''.$name.'\', $'.$this->getSingularName().', '.$this->asPhp($params).') ?]', $params) ?>
                <?php // echo $this->addCredentialCondition($this->getLinkToAction($name, $params, true), $params) ?>

            <?php endif; ?>
        <?php endforeach; ?>
    </div>
</td>
