[?php if ($value): ?]
    <span class=".glyphicon .glyphicon-ok-circle green"></span>
  [?php echo image_tag(sfConfig::get('sf_admin_module_web_dir').'/images/tick.png', array('alt' => __('Checked', array(), 'sf_admin'), 'title' => __('Checked', array(), 'sf_admin'))) ?]
[?php else: ?]
    <i class="icon-remove red"></i>
[?php endif; ?]
