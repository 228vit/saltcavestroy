            <?php foreach ($this->configuration->getValue('edit.actions') as $name => $params): ?>

                <?php if ('_list' == $name): ?>
                    <?php echo $this->addCredentialCondition('[?php echo $helper->linkToListAsModel('.$this->asPhp($params).') ?]', $params) ?>
                <?php endif; ?>

            <?php endforeach; ?>
