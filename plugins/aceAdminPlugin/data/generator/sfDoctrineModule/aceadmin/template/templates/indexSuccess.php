[?php use_helper('I18N', 'Date') ?]
[?php include_partial('<?php echo $this->getModuleName() ?>/assets') ?]

[?php slot('breadcrumbs') ?]
<ul class="breadcrumb">
    <li>
        <i class="icon-home home-icon"></i>
        <a href="#">Home</a>
    </li>
    <li class="">
        <a href="/">Dashboard</a>
    </li>
    <li class="active">
        <?php echo sfInflector::humanize($this->getModuleName()) ?>
    </li>
</ul>
[?php end_slot() ?]


    <div class="col-xs-12 page-header">
        <div class="col-sm-8">
            <h1>[?php echo <?php echo $this->getI18NString('list.title') ?> ?]</h1>
        </div>
        <div class="col-sm-4 align-right">
            <?php if (@$this->params['show_top_list_actions']): ?>
                [?php include_partial('<?php echo $this->getModuleName() ?>/list_actions', array('helper' => $helper)) ?]
            <?php endif; // show top batch actions ?>
        </div>
    </div>

<hr />

[?php include_partial('<?php echo $this->getModuleName() ?>/flashes') ?]

<div id="sf_admin_header">
    [?php include_partial('<?php echo $this->getModuleName() ?>/list_header', array('pager' => $pager)) ?]
</div>

<?php if ($this->configuration->hasFilterForm()): ?>
    <div id="sf_admin_bar">
        [?php include_partial('<?php echo $this->getModuleName() ?>/filters', array('form' => $filters, 'configuration'
        => $configuration)) ?]
    </div>
<?php endif; ?>


<div class="row">

    <div class="col-xs-12">

    <?php if ($this->configuration->getValue('list.batch_actions')): ?>
    <form
        action="[?php echo url_for('<?php echo $this->getUrlForAction('collection') ?>', array('action' => 'batch')) ?]"
        method="post">
    <?php endif; ?>


        [?php include_partial('<?php echo $this->getModuleName() ?>/list', array('pager' => $pager, 'sort' => $sort,
        'helper' => $helper)) ?]

        <div class="batch_actions">
            [?php include_partial('<?php echo $this->getModuleName() ?>/list_batch_actions', array('helper' => $helper)) ?]
            [?php include_partial('<?php echo $this->getModuleName() ?>/list_actions', array('helper' => $helper)) ?]
        </div>

    <?php if ($this->configuration->getValue('list.batch_actions')): ?>
    </form>
    <?php endif; ?>
    </div>
</div>

<div id="sf_admin_footer">
    [?php include_partial('<?php echo $this->getModuleName() ?>/list_footer', array('pager' => $pager)) ?]
</div>