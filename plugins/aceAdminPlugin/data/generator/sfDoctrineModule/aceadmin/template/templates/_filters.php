[?php use_stylesheets_for_form($form) ?]
[?php use_javascripts_for_form($form) ?]

[?php slot('filter_form') ?]

<div class="ace-settings-container" id="ace-settings-container">
    <div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
        <i class="icon-filter bigger-150"></i>
    </div>

    <div class="ace-settings-box" id="ace-settings-box">
        <div style="margin-bottom: 20px;">
            <h1>
                [?php echo sfInflector::classify($this->getModuleName()) ?]
                <small>
                    <i class="icon-double-angle-right"></i>
                    filter
                </small>
            </h1>
        </div>


        [?php if ($form->hasGlobalErrors()): ?]
        [?php echo $form->renderGlobalErrors() ?]
        [?php endif; ?]

        <form class="form-horizontal" action="[?php echo url_for('<?php echo $this->getUrlForAction('collection') ?>', array('action' => 'filter')) ?]" method="post">
            [?php if ($form->hasGlobalErrors()): ?]
            [?php echo $form->renderGlobalErrors() ?]
            [?php endif; ?]
            [?php foreach ($configuration->getFormFilterFields($form) as $name => $field): ?]
            [?php if ((isset($form[$name]) && $form[$name]->isHidden()) || (!isset($form[$name]) && $field->isReal())) continue ?]
            [?php include_partial('<?php echo $this->getModuleName() ?>/filters_field', array(
            'name' => $name,
            'attributes' => $field->getConfig('attributes', array()),
            'label' => $field->getConfig('label'),
            'help' => $field->getConfig('help'),
            'form' => $form,
            'field' => $field,
            'class' => 'sf_admin_form_row sf_admin_' . strtolower($field->getType()) . ' sf_admin_filter_field_' . $name,
            )) ?]
            [?php endforeach; ?]
            [?php echo $form->renderHiddenFields() ?]
            <div class="clearfix form-actions">
                <div class="col-md-offset-3 col-md-9">
                    <button class="btn btn-info" type="submit">
                        <i class="icon-ok bigger-110"></i>
                        Submit
                    </button>
                    &nbsp; &nbsp; &nbsp;
                    [?php echo link_to('<i class="icon-undo bigger-110"></i> Reset', '<?php echo $this->getUrlForAction('collection') ?>', array('action' => 'filter'), array('query_string' => '_reset', 'method' => 'post', 'class' => 'btn', 'type' => 'reset')) ?]
                </div>
            </div>


        </form>
    </div>

    <!-- /#ace-settings-container -->
</div>

[?php end_slot() ?]



