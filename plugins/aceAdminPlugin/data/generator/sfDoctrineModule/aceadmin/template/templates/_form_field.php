[?php if ($field->isPartial()): ?]
    [?php include_partial('<?php echo $this->getModuleName() ?>/'.$name, array('form' => $form, 'attributes' => $attributes instanceof sfOutputEscaper ? $attributes->getRawValue() : $attributes)) ?]
[?php elseif ($field->isComponent()): ?]
    [?php include_component('<?php echo $this->getModuleName() ?>', $name, array('form' => $form, 'attributes' => $attributes instanceof sfOutputEscaper ? $attributes->getRawValue() : $attributes)) ?]
[?php else: ?]
    <div class="form-group [?php $form[$name]->hasError() and print 'has-error' ?]">
        [?php echo $form[$name]->renderLabel($label, array('class' => 'col-sm-2 control-label no-padding-right')) ?]

        <div class="col-xs-12 col-sm-5">
                [?php
                    $html_class = $form[$name]->getWidget()->getAttribute('class');
                    switch (get_class($form[$name]->getWidget())){
                        case 'sfWidgetFormDoctrineChoice':
                            $class = '';
                            break;
                        case 'sfWidgetFormChoice':
                            $class = '';
                            break;
                        case 'sfWidgetFormDate':
                            $class = '';
                            break;
                        case 'sfWidgetFormDateTime':
                            $class = '';
                            break;
                        case 'sfWidgetFormTextarea':
                            $class = $html_class;
                            break;
                        case 'aceWidgetFormInputCheckbox':
                            $class = 'input-medium';
                            break;
                        default:
                            $class = 'width-100';
                    }
                    $attributes['class'] = $class;
                ?]

                [?php echo $form[$name]->render($attributes instanceof sfOutputEscaper ? $attributes->getRawValue() : $attributes) ?]

            [?php if ($help): ?]
                <span class="help-inline col-xs-12 col-sm-7">
                    <span class="middle">
                        [?php echo __($help, array(), '<?php echo $this->getI18nCatalogue() ?>') ?]
                    </span>
                </span>
            [?php elseif ($help = $form[$name]->renderHelp()): ?]
                <span class="help-inline col-sm-reset inline">
                    <span class="middle">
                        [?php echo $help ?]
                    </span>
                </span>
            [?php endif; ?]
        </div>
        [?php if ($form[$name]->hasError()): ?]
            <div class="help-block col-xs-12 col-sm-reset inline">
                [?php echo $form[$name]->renderError() ?]
            </div>
        [?php endif; ?]

    </div>
    <div class="space-2"></div>

[?php endif; ?]
