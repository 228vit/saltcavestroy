<?php if ($listActions = $this->configuration->getValue('list.batch_actions')): ?>
  <select name="batch_action">
    <option value="">---</option>
<?php foreach ((array) $listActions as $action => $params): ?>
    <?php echo $this->addCredentialCondition('<option value="'.$action.'">[?php echo \''.$params['label'].'\' ?]</option>', $params) ?>

<?php endforeach; ?>
  </select>
  [?php $form = new BaseForm(); if ($form->isCSRFProtected()): ?]
    <input type="hidden" name="[?php echo $form->getCSRFFieldName() ?]" value="[?php echo $form->getCSRFToken() ?]" />
  [?php endif; ?]
    <button type="submit" class="btn btn-primary">
        <i class="icon-rocket align-top bigger-125"></i>
        Run batch
    </button>
<?php endif; ?>
