[?php slot('breadcrumbs') ?]

<ul class="breadcrumb">
    <li>
        <i class="icon-home home-icon"></i>
        <a href="#">Home</a>
    </li>
    <li class="active">
        <a href="#">Dashboard</a>
    </li>
    <li class="">
        <a href="<?php echo $_SERVER['SCRIPT_NAME'].'/'. $this->getModuleName() ?>"><?php echo sfInflector::humanize($this->getModuleName()) ?></a>
    </li>
</ul>

[?php end_slot() ?]