  public function executeUpdate(sfWebRequest $request)
  {
    $this-><?php echo $this->getSingularName() ?> = $this->getRoute()->getObject();
    $this->form = $this->configuration->getForm($this-><?php echo $this->getSingularName() ?>);

    // in case if form invalid
    $this->prev = false;
    $this->next = false;

    $this->processForm($request, $this->form);

    $this->setTemplate('edit');
  }
