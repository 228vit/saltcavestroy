<?php

/**
 * aceAdminPlugin configuration.
 * 
 * @package     aceAdminPlugin
 * @subpackage  config
 * @author      228vit
 * @version     SVN: $Id: PluginConfiguration.class.php 17207 2009-04-10 15:36:26Z Kris.Wallsmith $
 */
class aceAdminPluginConfiguration extends sfPluginConfiguration
{
  const VERSION = '1.0.0-DEV';

  /**
   * @see sfPluginConfiguration
   */
  public function initialize()
  {
  }
}
